(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Metal_types
open Storage_types
include Local
include Current_version
open MMisc

let merge_account_history (account:account_js t) (history:account_history_js t option) : account_full_js t =
  let history, pending = match history with
    | None -> array [||], array [||]
    | Some o -> o##.history, o##.pending in
  object%js
    val index = account##.index
    val pkh = account##.pkh
    val manager = account##.manager
    val vault = account##.vault
    val name = account##.name
    val revealed = account##.revealed
    val managerKT = account##.managerKT
    val admin = account##.admin
    val history = history
    val pending = pending
  end

let merge_accounts_history (accs:account_js t js_array t) (o:accounts_history_entry t) =
  let l = to_list @@ unoptdef (array [||]) o##.history in
  of_list @@ to_listf (fun a ->
      let history = List.find_opt (fun o -> o##.pkh = a##.pkh) l in
      merge_account_history a history) accs

(* From js object to ocaml object *)

module Of_js = struct

  let optdef_exn f s = try f s with _ -> None
  let to_int i = int_of_string (to_string i)
  let to_int64 i = Int64.of_string (to_string i)
  let to_z i = Z.of_string (to_string i)

  let error (o: error_js t) = {
    err_kind = to_string o##.kind ;
    err_id = to_string o##.id ;
    err_info = to_string o##.info ;
  }

  let parameters (o: parameters_js t) = (
    to_optdef to_string o##.entrypoint,
    to_optdef to_string o##.value)

  let script (o: script_js t) = (
    to_optdef to_string o##.code,
    to_string o##.storage,
    to_optdef to_string o##.codeHash)

  let action (o: action_js t) = (to_string o##.name, to_string o##.arg)

  let manager_info (o: manager_info_js t) = {
    mi_source = to_string o##.source ;
    mi_fee = to_optdef to_int64 o##.fee ;
    mi_counter = to_optdef to_int o##.counter ;
    mi_gas_limit = to_optdef to_z o##.gasLimit ;
    mi_storage_limit = to_optdef to_z o##.storageLimit ;
    mi_burn = to_optdef to_int64 o##.burn ;
    mi_errors = to_optdef (to_listf error) o##.errors ;
    mi_failed = to_bool o##.failed ;
    mi_internal = to_bool o##.internal ;
  }

  let amount s_js currency =
    let s = String.lowercase_ascii @@ to_string s_js in
    let currency = to_optdef (fun s -> String.lowercase_ascii @@ to_string s) currency in
    if s = "max" || s = "maximum" then Int64.minus_one
    else match currency with
      | Some "dun" -> begin match float_of_string_opt s with
          | None -> log_str "cannot read amount"; assert false
          | Some f -> Int64.of_float @@ f *. 1_000_000. end
      | Some "udun" | Some "mudun" | None -> begin match Int64.of_string_opt s with
          | None -> log_str "cannot read amount"; assert false
          | Some i -> i end
      | Some c -> log "cannot handle currency %S" c; assert false

  let transaction_details (o: transaction_details_js t) =
    let trd_amount = amount o##.amount o##.currency in
    TraDetails {
      trd_dst = to_string o##.destination ;
      trd_amount;
      trd_parameters = to_optdef parameters o##.parameters ;
      trd_collect_call = to_bool o##.collectCall;
    }

  let origination_details (o: origination_details_js t) =
    OriDetails {
      ord_balance = amount o##.balance o##.currency ;
      ord_kt1 = to_optdef to_string o##.kt1 ;
      ord_delegate = to_optdef to_string o##.delegate ;
      ord_script = to_optdef script o##.script ;
    }

  let delegation_details (o: delegation_details_js t) =
    DelDetails (to_optdef to_string o##.delegate)

  let reveal_details (o: reveal_details_js t) =
    RvlDetails (to_string o##.pubkey)

  let manage_account_details (o: manage_account_details_js t) =
    let target = to_optdef to_string o##.target in
    match to_optdef to_string o##.json with
    | None ->
      ManDetails (target, MadDecoded {
          mao_maxrolls = if to_bool o##.set_maxrolls_ then Some (Optdef.to_option o##.maxrolls) else None ;
          mao_admin = if to_bool o##.set_admin_ then Some ((to_optdef to_string) o##.admin) else None;
          mao_white_list = to_optdef (to_listf to_string) o##.whiteList ;
          mao_delegation = to_optdef to_bool o##.delegation ;
          mao_recovery = if to_bool o##.set_recovery_ then Some (to_optdef to_string o##.recovery) else None;
          mao_actions = to_listf action o##.actions ;
        })
    | Some json -> ManDetails (target, MadJSON json)

  let manager_operation manager_info (o: 'a manager_operation_js t) =
    let trad = to_optdef transaction_details o##.transaction in
    let orid = to_optdef origination_details o##.origination in
    let deld = to_optdef delegation_details o##.delegation in
    let rvld = to_optdef reveal_details o##.reveal in
    let mand = to_optdef manage_account_details o##.manage_account_ in {
      mo_info = manager_info o##.info ;
      mo_det = match to_string o##.kind, trad, orid, deld, rvld, mand with
        | "transaction", Some trad, _, _, _, _ -> trad
        | "origination", _, Some orid, _, _, _ -> orid
        | "delegation", _, _, Some deld, _, _ -> deld
        | "reveal", _, _, _, Some rvld, _ -> rvld
        | "manage_account", _, _, _, _, Some mand -> mand
        | _ -> log_str "operation kind not handled"; assert false
    }

  let block_info (o: block_info_js t) = {
    bi_hash = to_string o##.hash ;
    bi_protocol = to_string o##.protocol ;
    bi_network = to_string o##.network ;
    bi_timestamp = to_string o##.timestamp ;
    bi_level = o##.level ;
  }

  let block_operation (o: block_operation_js t) = {
    bo_hash = to_string o##.hash ;
    bo_block = to_optdef block_info o##.block ;
    bo_branch = to_optdef to_string o##.branch ;
    bo_op = to_listf (manager_operation manager_info) o##.operations
  }

  let network n = Mhelpers.network_of_string (to_string n)

  let network_assoc f a =
    let a = to_list a in
    List.map
      (fun x -> network x##.network, f x##.content) a

  let vault (o: vault_js t) =
    match to_string o##.kind with
    | "ledger" -> begin match Optdef.to_option o##.path, Optdef.to_option o##.pwd with
        | path, Some pwd ->
          let pwd = Hex.to_bigstring (`Hex (to_string pwd)) in
          begin match path with
            | None -> Ledger ("44'/1729'/0'/0'", pwd)
            | Some path -> Ledger (to_string path, pwd) end
        | _ -> log "error: no password hash found"; assert false end
    | "local" -> begin match Optdef.to_option o##.iv, Optdef.to_option o##.esk with
        | Some iv, Some esk ->
          Local (Hex.to_bigstring (`Hex (to_string iv)),
                 Hex.to_bigstring (`Hex (to_string esk)))
        | _ -> log "error: no data for decryption"; assert false end
    | _ -> log "error: type of vault unknown"; assert false

  let notif_manager_info (o: notif_manager_info_js t) = {
    not_mi_fee = to_optdef to_int64 o##.fee ;
    not_mi_gas_limit = to_optdef to_z o##.gasLimit ;
    not_mi_storage_limit = to_optdef to_z o##.storageLimit ;
  }

  let notif_op (o: notif_js t) not_origin not_op =
    OpNot {
      not_origin;
      not_tsp = to_string o##.tsp;
      not_id = to_string o##.id;
      not_wid = o##.wid;
      not_network = to_optdef network o##.network;
      not_type = to_optdef to_string o##.type_;
      not_op
    }

  let notif_batch (o: notif_js t) not_origin not_op =
    BatchNot {
      not_origin;
      not_tsp = to_string o##.tsp;
      not_id = to_string o##.id;
      not_wid = o##.wid;
      not_network = to_optdef network o##.network;
      not_type = to_optdef to_string o##.type_;
      not_op
    }

  let notif_app (o: notif_js t) name url =
    let not_approv_id = to_string o##.id in
    let not_approv_wid = o##.wid in
    let not_approv_icon = convopt to_string @@ Optdef.to_option o##.icon in
    let not_approv_name = name in
    let not_approv_url = url in
    let not_approv_tsp = to_string o##.tsp in
    ApprovNot {
      not_approv_id ;
      not_approv_wid ;
      not_approv_icon ;
      not_approv_name ;
      not_approv_url ;
      not_approv_tsp ;
    }

  let notif (o: notif_js t) =
    match (to_string o##.kind), Optdef.to_option o##.op, Optdef.to_option o##.batch,
    Optdef.to_option o##.origin, Optdef.to_option o##.name, Optdef.to_option o##.url with
    | "approval", _, _, _, Some name, Some url -> notif_app o (to_string name) (to_string url)
    | s, Some op, _, Some origin, _, _ when s = "operation" || s = "add_to_batch" ->
      notif_op o (to_string origin) @@ manager_operation notif_manager_info op
    | s, _, Some a, Some origin, _, _ when s = "batch" || s = "commit_batch" -> notif_batch o (to_string origin) @@
      to_listf (manager_operation notif_manager_info) a
    | _ -> log "storage_utils: type of notification not recognised";
      assert false

  let acc_notif (o: notif_acc t) =
    let notif_acc = to_string o##.acc in
    let notifs = to_listf notif o##.ns in
    { notif_acc ; notifs }

  let account o =
    let pkh = to_string o##.pkh in
    let manager = to_string o##.manager in
    let name = to_string o##.name in
    let vault = vault o##.vault in
    let revealed = network_assoc to_string o##.revealed in
    let manager_kt = to_optdef to_bool o##.managerKT in
    let admin = network_assoc to_string o##.admin in
    {pkh ; manager ; name ; vault ; revealed; manager_kt; admin; pending =[];
     history = []; acc_index = o##.index }

  let accounts accs = to_listf account accs

  let accounts_entry (o: accounts_entry t) : account_js t js_array t =
    let keys = unoptdef_f [] to_list o##.accounts in
    of_listf (fun i ->
        Unsafe.get o (string ("account_" ^ (string_of_int i) ^ "_"))) keys

  let account_full (o: account_full_js t) =
    let account = account o in {
    account with
    pending = network_assoc (to_listf block_operation) o##.pending;
    history = network_assoc (to_listf block_operation) o##.history;
  }

  let accounts_full (accounts: account_js t js_array t) (o:accounts_history_entry t) =
    to_listf account_full (merge_accounts_history accounts o)

  let state (acc: account_full_js t) (net: js_string t) =
    {fo_acc = account_full acc; fo_net = network net}

  let network_entry (o: network_entry t) = network (unoptdef (string "Mainnet") o##.network)

  let custom_network (o: custom_network t) =
    let name, node, api = to_string o##.name, to_string o##.node, to_string o##.api in
    (name, node, api)

  let custom_networks (a: custom_network t js_array t) = to_listf custom_network a

  let custom_networks_entry (o: custom_networks_entry t) =
    unoptdef_f [] custom_networks o##.custom_networks

  let selected_entry (o: selected_entry t) =
    match Optdef.to_option o##.selected with
    | Some s when s != (string "") -> Some (to_string s)
    | _ -> None

  let notifs (a : notif_acc t js_array t) = to_listf acc_notif a
  let notifs_entry (n : notif_entry t) = unoptdef_f [] notifs n##.notifs

  let config (c: config_entry t) = {
    use_dunscan = unoptdef_f true to_bool c##.useDunscan;
    theme = unoptdef_f "default" to_string c##.theme;
    batch_notif = unoptdef_f false to_bool c##.batch_notif_;
    notif_timeout = unoptdef_f 20000. (fun n -> float_of_number n) c##.notif_timeout_;
  }
end

(* From ocaml object to js object *)

module To_js = struct

  let of_int i = string (string_of_int i)
  let of_int64 i = string (Int64.to_string i)
  let of_z i = string (Z.to_string i)

  let error e : error_js t =
    object%js
      val kind = string e.err_kind
      val id = string e.err_id
      val info = string e.err_info
    end

  let parameters (entrypoint, value) : parameters_js t =
    object%js
      val mutable entrypoint = optdef string entrypoint
      val mutable value = optdef string value
    end

  let script (code, storage, code_hash) : script_js t =
    object%js
      val mutable code = optdef string code
      val mutable storage = string storage
      val mutable codeHash = optdef string code_hash
    end

  let action (name, arg) : action_js t =
    object%js
      val mutable name = string name
      val mutable arg = string arg
    end

  let manager_info mi : manager_info_js t =
    object%js
      val mutable source = string mi.mi_source
      val mutable fee = optdef of_int64 mi.mi_fee
      val mutable counter = optdef of_int mi.mi_counter
      val mutable gasLimit = optdef of_z mi.mi_gas_limit
      val mutable storageLimit = optdef of_z mi.mi_storage_limit
      val mutable burn = optdef of_int64 mi.mi_burn
      val mutable errors = optdef (of_listf error) mi.mi_errors
      val mutable failed = bool mi.mi_failed
      val mutable internal = bool mi.mi_failed
    end

  let amount ?(currency="dun") i =
    let amount =
      if currency = "dun" then
        string @@ string_of_float (Int64.to_float i /. 1_000_000.)
      else if currency = "udun" || currency = "mudun" then
        string @@ Int64.to_string i
      else (log "currency %S not handled" currency; assert false)
    in
    amount, def (string currency)

  let transaction_details tr : transaction_details_js t =
    let amount, currency = amount tr.trd_amount in
    object%js
      val mutable destination = string tr.trd_dst
      val mutable amount = amount
      val mutable currency = currency
      val mutable parameters = optdef parameters tr.trd_parameters
      val mutable collectCall = bool tr.trd_collect_call
    end

  let origination_details ori : origination_details_js t =
    let balance, currency = amount ori.ord_balance in
    object%js
      val mutable balance = balance
      val mutable currency = currency
      val mutable kt1 = optdef string ori.ord_kt1
      val mutable delegate = optdef string ori.ord_delegate
      val mutable script = optdef script ori.ord_script
    end

  let delegation_details delegate : delegation_details_js t =
    object%js
      val mutable delegate = optdef string delegate
    end

  let reveal_details pubkey : reveal_details_js t =
    object%js
      val mutable pubkey = string pubkey
    end

  let manage_account_details ma : manage_account_details_js t =
    let maxrolls, set_maxrolls, admin, set_admin, whitelist, delegation,
        recovery, set_recovery, actions, json =
      match snd ma with
      | MadJSON json ->
        undefined, _false, undefined, _false, undefined, undefined,
        undefined, _false, array [||], def (string json)
      | MadDecoded mao ->
        let set_maxrolls, maxrolls = match mao.mao_maxrolls with
          | None -> _false, undefined
          | Some maxrolls -> _true, Optdef.option maxrolls in
        let set_admin, admin = match mao.mao_admin with
          | None -> _false, undefined
          | Some admin -> _true, optdef string admin in
        let set_recovery, recovery = match mao.mao_recovery with
          | None -> _false, undefined
          | Some recovery -> _true, optdef string recovery in
        maxrolls, set_maxrolls, admin, set_admin,
        optdef (of_listf string) mao.mao_white_list, optdef bool mao.mao_delegation,
        recovery, set_recovery, of_listf action mao.mao_actions, undefined in
    object%js
      val mutable target = optdef string (fst ma)
      val mutable maxrolls = maxrolls
      val mutable set_maxrolls_ = set_maxrolls
      val mutable admin = admin
      val mutable set_admin_ = set_admin
      val mutable whiteList = whitelist
      val mutable delegation = delegation
      val mutable recovery = recovery
      val mutable set_recovery_ = set_recovery
      val mutable actions = actions
      val mutable json = json
    end

  let manager_operation manager_info mo : 'a manager_operation_js t =
    let kind, tr, ori, del, rvl, man = match mo.mo_det with
      | TraDetails tr ->
        "transaction", def @@ transaction_details tr, undefined, undefined,
        undefined, undefined
      | OriDetails ori ->
        "origination", undefined, def @@ origination_details ori, undefined,
        undefined, undefined
      | DelDetails del ->
        "delegation", undefined, undefined, def @@ delegation_details del,
        undefined, undefined
      | RvlDetails rvl ->
        "reveal", undefined, undefined, undefined, def @@  reveal_details rvl,
        undefined
      | ManDetails man ->
        "manage_account", undefined, undefined, undefined, undefined,
        def @@ manage_account_details man in
    object%js
      val mutable info = manager_info mo.mo_info
      val mutable kind = string kind
      val mutable transaction = tr
      val mutable origination = ori
      val mutable delegation = del
      val mutable reveal = rvl
      val mutable manage_account_ = man
    end

  let block_info bi : block_info_js t =
    object%js
      val hash = string bi.bi_hash
      val protocol = string bi.bi_protocol
      val network = string bi.bi_network
      val timestamp = string bi.bi_timestamp
      val level = bi.bi_level
    end

  let block_operation bo : block_operation_js t =
    object%js
      val hash = string bo.bo_hash
      val block = optdef block_info bo.bo_block
      val branch = optdef string bo.bo_branch
      val operations = of_listf (manager_operation manager_info) bo.bo_op
    end

  let vault v : vault_js t =
    match v with
    | Ledger (path, pwd) ->
      object%js
        val kind = string "ledger"
        val path = def (string path)
        val pwd = def (string Hex.(show (of_bigstring pwd)))
        val iv = undefined
        val esk = undefined
      end
    | Local (iv, esk) ->
      object%js
        val kind = string "local"
        val iv = def (string Hex.(show (of_bigstring iv)))
        val esk = def (string Hex.(show (of_bigstring esk)))
        val path = undefined
        val pwd = undefined
      end

  let network n = string (Mhelpers.network_to_string n)

  let notif_manager_info mi : notif_manager_info_js t =
    object%js
      val mutable fee = optdef of_int64 mi.not_mi_fee
      val mutable gasLimit = optdef of_z mi.not_mi_gas_limit
      val mutable storageLimit = optdef of_z mi.not_mi_storage_limit
    end

  let notif no : notif_js t =
    let kind, origin, tsp, id, wid, op, batch , icon, name, url, network, type_ = match no with
      | OpNot n ->
        "operation", def (string n.not_origin), n.not_tsp, n.not_id, n.not_wid,
        def @@ manager_operation notif_manager_info n.not_op,
        undefined, undefined, undefined, undefined, optdef network n.not_network,
        optdef string n.not_type
      | BatchNot n ->
        "batch", def (string n.not_origin), n.not_tsp, n.not_id, n.not_wid, undefined,
        def @@ of_listf (manager_operation notif_manager_info) n.not_op,
        undefined, undefined, undefined, optdef network n.not_network,
        optdef string n.not_type
      | ApprovNot n ->
        "approval", undefined, n.not_approv_tsp, n.not_approv_id, n.not_approv_wid,
        undefined, undefined, optdef string n.not_approv_icon,
        def (string n.not_approv_name), def (string n.not_approv_url),
        undefined, undefined in
    object%js
      val mutable kind = string kind
      val mutable origin = origin
      val mutable tsp = string tsp
      val mutable id = string id
      val mutable wid = wid
      val mutable op = op
      val mutable batch = batch
      val mutable network = network
      val mutable type_ = type_
      val mutable icon = icon
      val mutable name = name
      val mutable url = url
    end

  let acc_notif {notif_acc ; notifs} : notif_acc t =
    object%js
      val acc = string notif_acc
      val ns = of_listf notif notifs
    end

  let network_assoc f a : 'a network_assoc_js t js_array t =
    of_listf (fun (n, content) ->
        object%js
          val content = f content
          val network = network n
        end) a

  let account a : account_js t =
    object%js
      val index = a.acc_index
      val pkh = string a.pkh
      val manager = string a.manager
      val name = string a.name
      val vault = vault a.vault
      val revealed = network_assoc string a.revealed
      val managerKT = optdef bool a.manager_kt
      val admin = network_assoc string a.admin
    end

  let account_full a : account_full_js t =
    object%js
      val index = a.acc_index
      val pkh = string a.pkh
      val manager = string a.manager
      val name = string a.name
      val vault = vault a.vault
      val revealed = network_assoc string a.revealed
      val managerKT = optdef bool a.manager_kt
      val admin = network_assoc string a.admin
      val pending = network_assoc (of_listf block_operation) a.pending
      val history = network_assoc (of_listf block_operation) a.history
    end

  let accounts accs = of_listf account accs
  let accounts_entry accs : accounts_entry t * account_js t Js_of_ocaml.Jstable.t =
    let accounts_table = Js_of_ocaml.Jstable.create () in
    let keys = List.map (fun acc ->
        Js_of_ocaml.Jstable.add accounts_table (string ("account_" ^ (string_of_int acc.acc_index))) (account acc);
        acc.acc_index) accs in
    object%js
      val accounts = def (array @@ Array.of_list keys)
    end,
    accounts_table

  let account_history a : account_history_js t =
    object%js
      val pkh = string a.pkh
      val pending = network_assoc (of_listf block_operation) a.pending
      val history = network_assoc (of_listf block_operation) a.history
    end

  let accounts_history l : accounts_history_entry t =
    object%js
      val history = def @@ of_listf account_history l
    end

  let network_entry n : network_entry t =
    object%js
      val network = def (network n)
    end

  let custom_network ~name ~node ~api : custom_network t =
    object%js
      val name = string name
      val node = string node
      val api = string api
    end

  let custom_networks networks =
    of_listf (fun (name, node, api) -> custom_network ~name ~node ~api) networks

  let custom_networks_entry networks : custom_networks_entry t =
    object%js
      val custom_networks = def @@ custom_networks networks
    end

  let selected_entry pkh : selected_entry t =
    object%js
      val selected = optdef string pkh
    end

  let notifs ns = of_listf acc_notif ns
  let notifs_entry ns : notif_entry t =
    object%js
      val notifs = def (notifs ns)
    end

  let config config : config_entry t =
    object%js
      val useDunscan = def (bool config.use_dunscan)
      val theme = def (string config.theme)
      val batch_notif_ = def (bool config.batch_notif)
      val notif_timeout_ = def (number_of_float config.notif_timeout)
    end
end

module Check_js = struct

  let clist f l =
    List.fold_left (fun acc x -> match acc, f x with
        | Error e, Ok _ | Ok _, Error e -> Error e
        | Error e, Error e2 -> Error (e @ e2)
        | Ok acc, Ok x -> Ok (x :: acc)) (Ok []) l

  let carray f a =
    clist f (to_list a) >|? fun l -> of_list l

  let coptdef f x = match Optdef.to_option x with
    | None -> Ok undefined
    | Some x -> f x

  let cpkh s_js =
    let s = to_string s_js in
    if MCrypto.check_pkh s then Ok s_js
    else Error [Str_err ("\"" ^ s ^ "\" is not a pkh")]

  let cint64 s =
    let s = to_string s in
    match Int64.of_string_opt s with
    | None -> Error [Str_err ("cannot read int64 \"" ^ s ^ "\"")]
    | Some i -> Ok (string @@ Int64.to_string i)

  let cfloat s =
    let s = to_string s in
    match float_of_string_opt s with
    | None -> Error [Str_err ("cannot read float \"" ^ s ^ "\"")]
    | Some i -> Ok (string @@ string_of_float i)

  let amount s_js currency_js =
    let s = String.lowercase_ascii @@ to_string s_js in
    let currency = to_optdef (fun s -> String.lowercase_ascii @@ to_string s) currency_js in
    if s = "max" || s = "maximum" then Ok (s_js, currency_js)
    else match currency with
      | None | Some "udun" | Some "mudun"-> cint64 s_js >|? fun i -> i, currency_js
      | Some "dun" -> cfloat s_js >|? fun f -> f, currency_js
      | Some c -> Error [Str_err ("cannot handle currency \"" ^ c) ]

  let cstring_exn f msg s_js =
    let s = to_string s_js in
    try ignore(f s); Ok s_js
    with _ -> Error [Str_err (msg s)]

  let cz s_js =
    cstring_exn Z.of_string (fun s -> "cannot read zarith \"" ^ s ^ "\"") s_js

  let script_expr ?contract s_js =
    let s = to_string s_js in
    match Dune.parse_script ?contract s with
    | Ok _ -> Ok s_js
    | Error e -> Error [e]

  let optdef_str f x = match to_optdef to_string x with
    | None -> Ok undefined
    | Some "" -> Ok undefined
    | Some x -> f (string x) >|? def

  type 'a check_res = ('a, metal_error list) result
  let parameters (o:parameters_js t) : parameters_js t optdef check_res =
    optdef_str ok o##.entrypoint >>? fun e ->
    optdef_str script_expr o##.value >|? fun v ->
    if e = undefined && v = undefined then undefined
    else def @@
      object%js
        val mutable entrypoint = e
        val mutable value = v
      end

  let script (o:script_js t) : script_js t optdef check_res =
    optdef_str (script_expr ~contract:true) o##.code >>? fun code ->
    optdef_str (script_expr ~contract:true) (def o##.storage) >>? fun storage ->
    optdef_str ok o##.codeHash >>? fun code_hash ->
    if code_hash <> undefined && code <> undefined then
      Error [Str_err "expected only one among: code, code_hash"]
    else match Optdef.to_option storage with
      | None -> Ok undefined
      | Some storage ->
        ok @@ def @@
        object%js
          val mutable code = code
          val mutable storage = storage
          val mutable codeHash = code_hash
        end

  let notif_manager_info (o:notif_manager_info_js t) : notif_manager_info_js t check_res =
    optdef_str cint64 o##.fee >>? fun fee ->
    optdef_str cz o##.gasLimit >>? fun gas_limit ->
    optdef_str cz o##.storageLimit >|? fun storage_limit ->
    object%js
      val mutable fee = fee
      val mutable gasLimit = gas_limit
      val mutable storageLimit = storage_limit
    end

  let transaction_details (o:transaction_details_js t) : transaction_details_js t check_res =
    cpkh o##.destination >>? fun dst ->
    amount o##.amount o##.currency >>? fun (amount, currency) ->
    coptdef parameters o##.parameters >|? fun parameters ->
    object%js
      val mutable destination = dst
      val mutable amount = amount
      val mutable currency = currency
      val mutable parameters = parameters
      val mutable collectCall = o##.collectCall
    end

  let origination_details (o:origination_details_js t) : origination_details_js t check_res =
    amount o##.balance o##.currency >>? fun (balance, currency) ->
    optdef_str cpkh o##.delegate >>? fun delegate ->
    coptdef script o##.script >|? fun script ->
    object%js
      val mutable balance = balance
      val mutable currency = currency
      val mutable kt1 = undefined
      val mutable delegate = delegate
      val mutable script = script
    end

  let reveal_details (o:reveal_details_js t) : reveal_details_js t check_res =
    if to_string o##.pubkey = "" then Error [Str_err "empty pubkey"]
    else Ok o

  let cunset f s_js = match to_optdef to_string s_js with
    | Some "unset" | Some "none" -> Ok (undefined, _true)
    | _ -> optdef_str f s_js >|? fun x -> x, _false

  let delegation_details (o:delegation_details_js t) : delegation_details_js t check_res =
    cunset cpkh o##.delegate >|? fun (delegate, _) ->
    object%js val mutable delegate = delegate end

  let manage_account_details (o:manage_account_details_js t) : manage_account_details_js t check_res =
    optdef_str cpkh o##.target >>? fun target ->
    optdef_str cpkh o##.admin >>? fun admin ->
    coptdef (fun a -> carray cpkh a >|? def) o##.whiteList >>? fun whitelist ->
    optdef_str cpkh o##.recovery >|? fun recovery ->
    object%js
      val mutable target = target
      val mutable maxrolls = o##.maxrolls
      val mutable set_maxrolls_ = bool (to_bool o##.set_maxrolls_ || o##.maxrolls <> undefined)
      val mutable admin = admin
      val mutable set_admin_ = bool (to_bool o##.set_admin_ || o##.admin <> undefined)
      val mutable whiteList = whitelist
      val mutable delegation = o##.delegation
      val mutable recovery = recovery
      val mutable set_recovery_ = bool (to_bool o##.set_recovery_ || o##.recovery <> undefined)
      val mutable actions = o##.actions
      val mutable json = o##.json
    end

  let manager_operation f (o:'a manager_operation_js t) : 'a manager_operation_js t check_res =
    f o##.info >>? fun info ->
    (match to_string o##.kind, Optdef.to_option o##.transaction,
           Optdef.to_option o##.origination, Optdef.to_option o##.delegation,
           Optdef.to_option o##.reveal, Optdef.to_option o##.manage_account_ with
     | "transaction", Some trd, _, _, _, _ -> transaction_details trd >|? fun trd ->
       def trd, undefined, undefined, undefined, undefined
     | "origination", _, Some ord, _, _, _ -> origination_details ord >|? fun ord ->
       undefined, def ord, undefined, undefined, undefined
     | "delegation", _, _, Some dlg, _, _ -> delegation_details dlg >|? fun dlg ->
       undefined, undefined, def dlg, undefined, undefined
     | "reveal", _, _, _, Some rvl, _ -> reveal_details rvl >|? fun rvl ->
       undefined, undefined, undefined, def rvl, undefined
     | "manage_account", _, _, _, _, Some man -> manage_account_details man >|? fun man ->
       undefined, undefined, undefined, undefined, def man
     | _ -> Ok (undefined, undefined, undefined, undefined, undefined))
    >|? fun (trd, ord, dlg, rvl, man) ->
    object%js
      val mutable kind = o##.kind
      val mutable info = info
      val mutable transaction = trd
      val mutable origination = ord
      val mutable delegation = dlg
      val mutable reveal = rvl
      val mutable manage_account_ = man
    end

end
