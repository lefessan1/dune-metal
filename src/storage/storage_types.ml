(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Js

class type version_entry = object
  method version : int optdef prop
end

class type ['a] network_assoc_js = object
  method content : 'a readonly_prop
  method network : js_string t readonly_prop
end

module V0 = struct

  class type transaction_js = object
    method hash : js_string t prop
    method src : js_string t prop
    method fee : js_string t prop
    method dst : js_string t prop
    method amount : js_string t prop
    method counter : js_string t prop
    method gasLimit : js_string t prop
    method storageLimit : js_string t prop
    method entrypoint : js_string t optdef prop
    method parameters : js_string t optdef prop
    method failed : js_string t prop
    method internal : js_string t prop
    method burn : js_string t prop
    method opLevel : js_string t prop
    method tsp : js_string t prop
    method status : js_string t prop
    method errors : 'a js_array t optdef prop
  end

  class type vault_js = object
    method kind : js_string t readonly_prop
    method iv : js_string t optdef readonly_prop
    method esk : js_string t optdef readonly_prop
    method path : js_string t optdef readonly_prop
    method pwd : js_string t optdef readonly_prop
  end

  class type account_js0 = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : js_string t optdef prop
    method pending : transaction_js t js_array t prop
  end

  class type accounts_entry0 = object
    method accounts : account_js0 t js_array t optdef prop
  end

  class type approved_entry = object
    method approved : js_string t js_array t optdef prop
  end

  class type state_entry = object
    method state : js_string t prop
  end

  class type network_entry = object
    method network : js_string t optdef readonly_prop
  end

  class type custom_network = object
    method name : js_string t readonly_prop
    method node : js_string t readonly_prop
    method api : js_string t readonly_prop
  end
  class type custom_networks_entry = object
    method custom_networks : custom_network t js_array t optdef readonly_prop
  end

  class type selected_entry = object
    method selected : js_string t optdef readonly_prop
  end


end

module V1 = struct

  include V0

  class type network_assoc_js1 = object
    method hash : js_string t prop
    method network : js_string t prop
  end

  class type account_js1 = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : network_assoc_js1 t js_array t prop
    method pending : transaction_js t js_array t prop
  end

  class type accounts_entry1 = object
    method accounts : account_js1 t js_array t optdef prop
  end
end

module V2 = struct

  include V1

  class type account_js2 = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : network_assoc_js1 t js_array t prop
    method pending : transaction_js t js_array t prop
    method managerKT : bool t optdef prop
    method admin : network_assoc_js1 t js_array t prop
  end

  class type accounts_entry2 = object
    method accounts : account_js2 t js_array t optdef prop
  end
end

module V3 = struct
  include V2

  class type account_js3 = object
    method pkh : js_string t readonly_prop
    method manager : js_string t readonly_prop
    method vault : vault_js t readonly_prop
    method name : js_string t readonly_prop
    method revealed : js_string t network_assoc_js t js_array t readonly_prop
    method managerKT : bool t optdef readonly_prop
    method admin : js_string t network_assoc_js t js_array t readonly_prop
  end

  class type accounts_entry3 = object
    method accounts : account_js3 t js_array t optdef readonly_prop
  end

  class type config_entry3 = object
    method useDunscan : bool t optdef readonly_prop
  end
end

module V4 = struct
  include V3

  class type config_entry = object
    method useDunscan : bool t optdef readonly_prop
    method theme : js_string t optdef readonly_prop
    method batch_notif_ : bool t optdef readonly_prop
    method notif_timeout_ : number t optdef readonly_prop
  end
end

module V5 = struct
  include V4

  class type accounts_entry = object
    method accounts : int js_array t optdef readonly_prop
  end

  class type account_js = object
    method index : int readonly_prop
    method pkh : js_string t readonly_prop
    method manager : js_string t readonly_prop
    method vault : vault_js t readonly_prop
    method name : js_string t readonly_prop
    method revealed : js_string t network_assoc_js t js_array t readonly_prop
    method managerKT : bool t optdef readonly_prop
    method admin : js_string t network_assoc_js t js_array t readonly_prop
  end
end

let current_version = 6
module Current_version = V5

module Local = struct

  class type error_js = object
    method kind : js_string t readonly_prop
    method id : js_string t readonly_prop
    method info : js_string t readonly_prop
  end

  class type parameters_js = object
    method entrypoint : js_string t optdef prop
    method value : js_string t optdef prop
  end

  class type script_js = object
    method code : js_string t optdef prop
    method storage : js_string t prop
    method codeHash : js_string t optdef prop
  end

  class type action_js = object
    method name : js_string t prop
    method arg : js_string t prop
  end

  class type manager_info_js = object
    method source : js_string t prop
    method fee : js_string t optdef prop
    method counter : js_string t optdef prop
    method gasLimit : js_string t optdef prop
    method storageLimit : js_string t optdef prop
    method burn : js_string t optdef prop
    method errors : error_js t js_array t optdef prop
    method failed : bool t prop
    method internal : bool t prop
  end

  class type transaction_details_js = object
    method destination : js_string t prop
    method amount : js_string t prop
    method currency : js_string t optdef prop
    method parameters : parameters_js t optdef prop
    method collectCall : bool t prop
  end

  class type origination_details_js = object
    method balance : js_string t prop
    method currency : js_string t optdef prop
    method kt1 : js_string t optdef prop
    method delegate : js_string t optdef prop
    method script : script_js t optdef prop
  end

  class type reveal_details_js = object
    method pubkey : js_string t prop
  end

  class type delegation_details_js = object
    method delegate : js_string t optdef prop
  end

  class type manage_account_details_js = object
    method target : js_string t optdef prop
    method maxrolls : int optdef prop
    method set_maxrolls_ : bool t prop
    method admin : js_string t optdef prop
    method set_admin_ : bool t prop
    method whiteList : js_string t js_array t optdef prop
    method delegation : bool t optdef prop
    method recovery : js_string t optdef prop
    method set_recovery_ : bool t prop
    method actions : action_js t js_array t prop
    method json : js_string t optdef prop
  end

  class type ['a] manager_operation_js = object
    method kind : js_string t prop
    method info : 'a prop
    method transaction : transaction_details_js t optdef prop
    method origination : origination_details_js t optdef prop
    method reveal : reveal_details_js t optdef prop
    method delegation : delegation_details_js t optdef prop
    method manage_account_ : manage_account_details_js t optdef prop
  end

  class type block_info_js = object
    method protocol : js_string t readonly_prop
    method network : js_string t readonly_prop
    method hash : js_string t readonly_prop
    method timestamp : js_string t readonly_prop
    method level : int readonly_prop
  end

  class type block_operation_js = object
    method hash : js_string t readonly_prop
    method block : block_info_js t optdef readonly_prop
    method branch : js_string t optdef readonly_prop
    method operations : manager_info_js t manager_operation_js t js_array t readonly_prop
  end


  class type notif_manager_info_js = object
    method fee : js_string t optdef prop
    method gasLimit : js_string t optdef prop
    method storageLimit : js_string t optdef prop
  end

  class type notif_js = object
    method kind : js_string t prop
    method id : js_string t prop
    method wid : int prop
    method tsp : js_string t prop
    method origin : js_string t optdef prop
    (* Operation *)
    method op : notif_manager_info_js t manager_operation_js t optdef prop
    (* Batch *)
    method batch : notif_manager_info_js t manager_operation_js t js_array t optdef prop
    (* Operation and Batch *)
    method network : js_string t optdef prop
    method type_ : js_string t optdef prop
    (* Approv *)
    method icon : js_string t optdef prop
    method name : js_string t optdef prop
    method url : js_string t optdef prop
  end

  class type notif_acc = object
    method acc : js_string t readonly_prop
    method ns : notif_js t js_array t readonly_prop
  end

  class type notif_entry = object
    method notifs : notif_acc t js_array t optdef readonly_prop
  end

  class type account_history_js = object
    method pkh : js_string t readonly_prop
    method pending : block_operation_js t js_array t network_assoc_js t js_array t readonly_prop
    method history : block_operation_js t js_array t network_assoc_js t js_array t readonly_prop
  end

  class type accounts_history_entry = object
    method history : account_history_js t js_array t optdef readonly_prop
  end

  class type account_full_js = object
    inherit Current_version.account_js
    method pending : block_operation_js t js_array t network_assoc_js t js_array t readonly_prop
    method history : block_operation_js t js_array t network_assoc_js t js_array t readonly_prop
  end
end
