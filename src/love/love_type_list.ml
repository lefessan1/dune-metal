(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_ast_types
open Love_ast_types.TYPE
open Love_type

type default_type_def =
    Core of type_var list * traits
  | TypeDef of typedef

type core_type = {
  ct_typedef : default_type_def;
  ct_ty : TYPE.t list -> TYPE.t
}

let def_type_to_type dt params = dt.ct_ty params

(* GLOBAL_STATE *)
let all_types : core_type Collections.StringMap.t ref = ref Collections.StringMap.empty

let register name tdef =
  all_types := Collections.StringMap.add name tdef !all_types

let get_type name params : t =
  def_type_to_type (
    match Collections.StringMap.find_opt name !all_types with
      None -> raise (Exceptions.BadArgument ("LTL : Unknown type " ^ name))
    | Some t -> t
  )
    params

let exists name : bool = Collections.StringMap.mem name !all_types

let is_core_type = function
  | Ident.LName n -> exists n
  | Ident.LDot _ -> false

let fold f acc =
  Collections.StringMap.fold
    f
    !all_types
    acc

let core_type id args traits =
  let type_id = Ident.create_id id in
  let ct_typedef = Core (args, traits) in
  let ct_ty = let args_size = List.length args in
    fun targs ->
      let targs_size = (List.length targs)
      in
      if Compare.Int.equal targs_size args_size
      then TUser (type_id, targs)
      else raise  (
          Exceptions.BadArgument (
            Format.asprintf
              "Type %s should have type %i arguments, but it is given %i"
              id
              args_size
              targs_size
          )
        )
  in
  {ct_typedef; ct_ty}

let register_core id args traits = register id (core_type id args traits)

(** Option *)
let tvo = {tv_name =  "@'opt"; tv_traits = default_trait}

let opt_def =
  TypeDef (
    SumType {
      sparams = [tvo];
      srec = NonRec;
      scons = [
        "Some", [TVar tvo];
        "None", []
      ]
    }
  )

let opt_ty =
  function
    [t] -> TUser (Ident.create_id "option", [t])
  | _ -> raise (Exceptions.BadArgument "Type option should have exactly 1 argument")

let option = {
  ct_typedef = opt_def;
  ct_ty = opt_ty
}

(** Variant *)
let tvl = {tv_name =  "@'left"; tv_traits = default_trait}
let tvr = {tv_name =  "@'right"; tv_traits = default_trait}

let var_def =
  TypeDef (
    SumType {
      sparams = [tvl; tvr];
      srec = NonRec;
      scons = [
        "Left", [TVar tvl];
        "Right", [TVar tvr]
      ]
    }
  )

let var_ty =
  function
    [tl; tr] -> TUser (Ident.create_id "variant", [tl; tr])
  | _ -> raise (Exceptions.BadArgument "Type option should have exactly 1 argument")

let variant = {
  ct_typedef = var_def;
  ct_ty = var_ty
}

let tv = Love_type.fresh_typevar

(** Type registration *)
let init () =
  all_types := Collections.StringMap.empty ;

  register_core "unit" [] comparable;
  register_core "bool" [] comparable;
  register_core "int" [] comparable;
  register_core "nat" [] comparable;
  register_core "bytes" [] comparable;
  register_core "string" [] comparable;
  register_core "dun" [] comparable;
  register_core "key" [] comparable;
  register_core "keyhash" [] comparable;
  register_core "signature" [] comparable;
  register_core "timestamp" [] comparable;
  register_core "address" [] comparable;

  register_core "operation" [] default_trait;

  register_core "list" [tv ~name:"@'list_elt" ~tv_traits:default_trait ()]
    (if Love_pervasives.has_protocol_revision 3 then comparable else default_trait);
  register_core "set" [tv ~name:"@'set_elt" ~tv_traits:comparable ()] comparable;
  register_core
    "map"
    [tv ~name:"@'map_key" ~tv_traits:comparable ();
     tv ~name:"@'map_key" ~tv_traits:default_trait ()]
    (if Love_pervasives.has_protocol_revision 3 then comparable else default_trait);

  register_core
    "bigmap"
    [tv ~name:"@'bigmap_key" ~tv_traits:default_trait ();
     tv ~name:"@'bigmap_key" ~tv_traits:default_trait ()]
    default_trait;

  register_core
    "entrypoint"
    [tv ~name:"@'entry_param" ~tv_traits:default_trait ();]
    default_trait;

  register_core
    "view"
    [tv ~name:"@'view_fun" ~tv_traits:default_trait ();]
    default_trait;

  register "option" option;
  register "variant" variant
