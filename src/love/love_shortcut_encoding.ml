module MBytes = MBytes

module Data_encoding = struct
  type json =
    [ `O of (string * json) list
    | `Bool of bool
    | `Float of float
    | `A of json list
    | `Null
    | `String of string ]

  module Json = Json_encoding
  include Json
  let json = any_ezjson_value
  let int31 = int
  let bytes = conv
      (fun h -> Hex.show @@ MBytes.to_hex h)
      (fun s -> MBytes.of_hex (`Hex s)) string
  let z = conv Z.to_string Z.of_string string

  module Tez_repr = struct
    let encoding = conv Int64.to_string Int64.of_string string
  end

  module Ztez_repr = struct
    let encoding = conv Z.to_string Z.of_string string
  end

  let n = Ztez_repr.encoding

  module Signature = struct
    module Public_key = struct
      let encoding = string
    end
    module Public_key_hash = struct
      let encoding = string
    end
    let encoding = string
  end

  module Script_timestamp_repr = struct
    let to_zint t = t
    let of_zint t = t
  end

  module Contract_repr = struct
    let encoding = string
    let of_b58check s = Ok s
    let to_b58check s = s
  end

  module Script_expr_hash = struct
    let encoding = string
  end
end

module Love_lexer = struct
  let parse_top_contract _ = assert false
  let parse_value _ = assert false
end
