(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal
open Types
open MLwt

module NodeS = Node.Services
module Node = Node.Make(EzXhr_lwt)

module ENode = struct
  let mainnet_node_url = EzAPI.TYPES.BASE (Printf.sprintf "https://mainnet-node.%s/" Menv.external_host)
  let testnet_node_url = EzAPI.TYPES.BASE (Printf.sprintf "https://testnet-node.%s/" Menv.external_host)
  let devnet_node_url = EzAPI.TYPES.BASE (Printf.sprintf "https://devnet-node.%s/" Menv.external_host)

  let get_node_base_url ?error network f = match network with
    | Mainnet -> f mainnet_node_url
    | Testnet -> f testnet_node_url
    | Devnet  -> f devnet_node_url
    | Custom name -> Storage_reader.get_custom_networks (fun l ->
        match List.find_opt (fun (n, _node, _api) -> name = n) l with
        | None ->
          let s = Printf.sprintf "Unknown network %S" name in
          Ezjs_min.log_str s;
          begin match error with
            | Some e -> e 400 (Some s)
            | None -> () end
        | Some (_, node, _) -> f (EzAPI.TYPES.BASE node))

  let get_node_base_url_lwt = function
    | Mainnet -> return (Ok mainnet_node_url)
    | Testnet -> return (Ok testnet_node_url)
    | Devnet  -> return (Ok devnet_node_url)
    | Custom name ->
      let (promise, resolver) = Lwt.wait () in
      Storage_reader.get_custom_networks (fun l ->
          match List.find_opt (fun (n, _node, _api) -> name = n) l with
          | None -> Lwt.wakeup resolver
                      (Error (Str_err (Printf.sprintf "Unknown network %S" name)))
          | Some (_, node, _) -> Lwt.wakeup resolver (Ok (EzAPI.TYPES.BASE node)));
      promise

  let get_account_info ?(network=Mainnet) ?error pkh f =
    get_node_base_url ?error network (fun base ->
        async_req ?error (Node.get_account_info ~base pkh) f)

  let get_entrypoints ?(network=Mainnet) ?error pkh f =
    get_node_base_url ?error network (fun base ->
        async_req ?error (Node.get_entrypoints ~base pkh) f)

  let get_accounts_lwt () =
    let (promise, resolver) = Lwt.wait () in
    Storage_reader.get_accounts (Lwt.wakeup resolver);
    promise

  let forge_manager_operations ~state ops =
    let get_pk () = Vault.Lwt.pk_of_vault state.fo_acc in
    get_node_base_url_lwt state.fo_net >>=? fun base ->
    Node.forge_manager_operations ~base ~get_pk ~src:state.fo_acc.pkh ops

  let forge_empty_account ~state dst =
    let get_pk () = Vault.Lwt.pk_of_vault state.fo_acc in
    get_node_base_url_lwt state.fo_net >>=? fun base ->
    Node.forge_empty_account ~base ~get_pk ~src:state.fo_acc.pkh dst

  let send_manager_operations ?error ?callback ~state fg =
    async_req ?error (
      get_node_base_url_lwt state.fo_net >>=? fun base ->
      Node.inject ~base ~sign:(Vault.Lwt.sign state.fo_acc) fg >>=? fun op_bytes ->
      let op_hash = Crypto.Operation_hash.b58enc op_bytes in
      let (r, p) = Lwt.wait () in
      let bo_op = List.rev @@ List.fold_left (fun acc x ->
          match From_dune.dune_op ~op_bytes:fg.fg_bytes x with
          | None -> acc
          | Some x -> x :: acc) [] fg.fg_ops in
      let bo = {bo_hash = op_hash; bo_block = None; bo_branch = None; bo_op} in
      let callback acc accs =
        (match callback with None -> () | Some cb -> cb acc accs);
        Lwt.wakeup p (op_hash, op_bytes, bo_op) in
      Storage_writer.add_pending ~callback state.fo_net state.fo_acc.pending
        bo state.fo_acc;
      r >>= fun x -> Lwt.return (Ok x))

  let send_activation ?(network=Mainnet) ?error pkh secret f =
    get_node_base_url ?error network (fun base ->
        async_req ?error
          (Node.forge_activation ~base ~pkh secret >>=? fun (op, ops) ->
           Node.sign_zero_operation op >>=? fun op ->
           Node.silent_inject ~base op >>|? fun h ->
           h, ops
          ) f)

  let get_admin ~state accounts = match List.assoc_opt state.fo_net state.fo_acc.admin with
    | None -> Ok None
    | Some admin -> match List.find_opt (fun a -> a.pkh = admin) accounts with
      | None -> Error (Str_err (
          Printf.sprintf "You are not admin (%s) for account %s"
            admin state.fo_acc.pkh))
      | Some a -> Ok (Some a)

  let admin_sign ~state accounts bytes =
    match get_admin ~state accounts with
    | Ok (Some admin) -> Vault.Lwt.sign admin bytes
    | Ok _ -> Vault.Lwt.sign state.fo_acc bytes
    | Error e -> return (Error e)

  let get_revealed ?error ~state f =
    get_node_base_url ?error state.fo_net (fun base ->
        async_req ?error (Node.get_manager_key ~base state.fo_acc.pkh) f)

end

module Scan = struct

  let mainnet_dunscan_url = EzAPI.TYPES.BASE ("https://api." ^ Menv.external_host)
  let testnet_dunscan_url = EzAPI.TYPES.BASE ("https://api.testnet." ^ Menv.external_host)
  let devnet_dunscan_url = EzAPI.TYPES.BASE ("https://api.devnet." ^ Menv.external_host)

  let get_dunscan_base_url ?error network f = match network with
    | Mainnet -> f mainnet_dunscan_url
    | Testnet -> f testnet_dunscan_url
    | Devnet  -> f devnet_dunscan_url
    | Custom name -> Storage_reader.get_custom_networks (fun l ->
        match List.find_opt (fun (n, _node, _api) -> name = n) l with
        | None ->
          let s = Printf.sprintf "Unknown network %S" name in
          Ezjs_min.log_str s;
          begin match error with
            | Some e -> e 400 (Some s)
            | None -> () end
        | Some (_, _, api) -> f (EzAPI.TYPES.BASE api))

  let remaining_pending ~state ~kind ops =
    let pending =
      Mhelpers.get_network_assoc [] state.fo_net state.fo_acc.pending in
    let kind_pending = List.filter (fun o -> Mhelpers.is_op_kind kind (List.hd o.bo_op).mo_det) pending in
    let remaining_pending, new_history = Mhelpers.split_mem
        (fun bo_pending -> List.find_opt
            (fun bo -> bo.bo_hash = bo_pending.bo_hash) ops)
        kind_pending in
    let has_changed =
      List.length new_history <> 0 ||
      List.length kind_pending <> List.length remaining_pending in
    remaining_pending, new_history, has_changed

  let update_pending_history ~refresh ~state ~kind ops f =
    let remaining_pending, new_history, has_changed = remaining_pending ~state ~kind ops in
    if not has_changed then f (remaining_pending @ ops)
    else
      let history = Mhelpers.get_network_assoc [] state.fo_net state.fo_acc.history in
      Storage_writer.update_account
        ~callback:(fun acc accs ->
            refresh acc accs;
            f (remaining_pending @ ops)) {
        state.fo_acc with
        pending = Mhelpers.update_network_assoc state.fo_net state.fo_acc.pending
            remaining_pending;
        history = Mhelpers.update_network_assoc state.fo_net state.fo_acc.history
            (new_history @ history) }

  let param_page = EzAPI.Param.int "p"
  let param_page_size = EzAPI.Param.int "number"
  let page_args ~page ~page_size args =
    [param_page, EzAPI.TYPES.I page; param_page_size, EzAPI.TYPES.I page_size] @ args

  let get1 ?error ?params ?headers ?msg ~state service arg f =
    get_dunscan_base_url ?error state.fo_net (fun base ->
        async_req ?error (Node.get1 ?params ?msg ?headers base service arg) f)

  let get0 ?error ?params ?headers ?msg ~state service f =
    get_dunscan_base_url ?error state.fo_net (fun base ->
        async_req ?error (Node.get0 ?params ?msg ?headers base service) f)

  module S = struct
    let arg_hash = NodeS.arg_hash
    let param_type = EzAPI.Param.string "type"
    let param_to = EzAPI.Param.string "to_is"
    let param_from = EzAPI.Param.string "from_is"

    let nb_operations : (string, int) NodeS.service1 =
      EzAPI.service
        ~params:[param_type]
        ~output:EzEncoding.tup1_int
        EzAPI.Path.(root // "v3" // "number_operations" /: arg_hash)

    let operations : (string, Data_types_min.operation list) NodeS.service1 =
      EzAPI.service
        ~params:[param_type; param_page; param_page_size]
        ~output:(Api_encoding_min.V1.Op.operations false)
        EzAPI.Path.(root // "v3" // "operations_nomic" /: arg_hash)

    let operation : (string, Data_types_min.operation) NodeS.service1 =
    EzAPI.service
      ~params:[]
      ~output:(Api_encoding_min.V1.Op.operation false)
      EzAPI.Path.(root // "v3" // "operation" /: arg_hash)

    let services : service list NodeS.service0 =
      EzAPI.service
        ~output:Metal_encoding.dunscan_services
        EzAPI.Path.(root // "v3" // "services")

    let nb_search_transactions : int NodeS.service0 =
      EzAPI.service
        ~params:[param_to; param_from]
        ~output:EzEncoding.tup1_int
        EzAPI.Path.(root // "v3" // "nb_adv_search" // "search_transactions")

    let search_transactions : (unit * Data_types_min.operation list) NodeS.service0 =
      EzAPI.service
        ~params:[param_to; param_from; param_page; param_page_size]
        ~output:Metal_encoding.adv_transactions_encoding
        EzAPI.Path.(root // "v3" // "adv_search" // "search_transactions")

  end

  let nb_notarizations ?error ~state f =
    match Mhelpers.notarization_contract state.fo_net with
    | None -> f 0
    | Some contract ->
      get0 ?error ~params:[S.param_to, EzAPI.TYPES.S contract;
                          S.param_from, EzAPI.TYPES.S state.fo_acc.pkh]
        ~state S.nb_search_transactions f

  let get_notarizations_page ?error ?(page=0) ?(page_size=20) ~state ~refresh ~kind filter f =
    match Mhelpers.notarization_contract state.fo_net with
    | None -> f []
    | Some contract ->
      let params = page_args ~page ~page_size
          [S.param_to, EzAPI.TYPES.S contract; S.param_from, EzAPI.TYPES.S state.fo_acc.pkh] in
      get0 ?error ~params ~state S.search_transactions
        (fun ((), l) ->
           let ops = From_dunscan.dunscan_ops l in
           update_pending_history ~refresh ~state ~kind ops
             (fun ops -> f Mhelpers.(flatten_block_operations filter ops)))

  let nb_operations ?error ~state kind =
    if kind = "Notarization" then
      nb_notarizations ?error ~state
    else
      get1 ?error ~params:[S.param_type, EzAPI.TYPES.S (String.capitalize_ascii kind)]
        ~state S.nb_operations state.fo_acc.pkh

  let get_operations_page ?error ?(page=0) ?(page_size=20) ?(manager=false) ~state ~refresh
      filter kind f =
    let pkh = if manager then state.fo_acc.manager else state.fo_acc.pkh in
    let params = page_args ~page ~page_size
        [S.param_type, EzAPI.TYPES.S (String.capitalize_ascii kind)] in
    if kind = "Notarization" then
      get_notarizations_page ?error ~page ~page_size ~state ~refresh ~kind filter f
    else
      get1 ?error ~params
        ~state S.operations pkh
        (fun l ->
           let ops = From_dunscan.dunscan_ops l in
           update_pending_history ~refresh ~state ~kind ops
             (fun ops -> f @@ Mhelpers.flatten_block_operations filter ops))

  let page_wrap ?error ?title ?suf_id ?page_sizer ~state ~refresh
      filter kind update =
    nb_operations ?error ~state kind (fun n ->
        update ?title ?suf_id ?page_sizer ?state:(Some state) n (fun page page_size f ->
            get_operations_page ?error ~page ~page_size ~state ~refresh
              filter kind f))

  let page_wrap_simple ?error ?page ?page_size ~state ~refresh filter kind update =
    nb_operations ?error ~state kind @@ fun n ->
    get_operations_page ?error ?page ?page_size ~state ~refresh filter kind @@ fun ops ->
    update n ops

  let get_activated ?error ~state f =
    get1 ?error ~params:[S.param_type, EzAPI.TYPES.S "Activation"]
      ~state S.operations state.fo_acc.pkh
      (function
        | Data_types_min.{op_type = Anonymous (Activation _ :: _); _} :: _ -> f true
        | _ -> f false)

  let get_services ?error ~state f = get0 ?error ~state S.services f

  let update_pending ?error ~state f =
    let pendings =
      Mhelpers.get_network_assoc [] state.fo_net state.fo_acc.pending in
    let rec aux = function
      | [] -> Storage_reader.get_account_full_js f
      | hd :: tl ->
        get1 ?error ~state S.operation hd.bo_hash
          (function op ->
             let new_pendings =
               List.filter (fun p -> p.bo_hash <> op.Data_types_min.op_hash)
                 pendings in
             Storage_writer.update_pendings
               ~callback:(fun _acc _accs -> aux tl)
               state.fo_net
               new_pendings
               state.fo_acc) in
    aux pendings
end

module Storage = struct

  let get_operations ?(manager=false) ~state ~refresh filter _kind f =
    ignore(refresh);
    let aux account =
      Mhelpers.get_network_assoc [] state.fo_net account.pending @
      Mhelpers.get_network_assoc [] state.fo_net account.history |>
      (Mhelpers.flatten_block_operations filter) |> f in
    if manager then
      Storage_reader.get_accounts (fun l ->
          match List.find_opt (fun {pkh; _} -> pkh = state.fo_acc.manager) l with
          | None -> aux state.fo_acc
          | Some account -> aux account)
    else
      aux state.fo_acc

  let get_operations_page ?(page=0) ?(page_size=20) ?manager ~state ~refresh
      filter _kind f =
    let start, length = page * page_size, page_size in
    get_operations ?manager ~state ~refresh filter _kind
      (fun l -> f @@ sublist ~start length l)

  let page_wrap ?title ?suf_id ?page_sizer ~state ~refresh filter kind update =
    get_operations ~state ~refresh filter kind (fun ops ->
        let n = List.length ops in
        update ?title ?suf_id ?page_sizer ?state:(Some state) n (fun page page_size f ->
            let start, length = page * page_size, page_size in
            let ops = sublist ~start length ops in
            f ops))

  let page_wrap_simple ?(page=0) ?(page_size=20) ~state ~refresh filter kind update =
    get_operations ~state ~refresh filter kind @@ fun ops ->
    let n = List.length ops in
    let start, length = page * page_size, page_size in
    let ops = sublist ~start length ops in
    update n ops

  (* let get_notarizations ?title ?suf_id ?page_sizer ~state ~refresh update =
   *   match Mhelpers.notarization_contract state.fo_net with
   *   | None -> update ?title ?suf_id ?page_sizer ?state:(Some state) 0 (fun _page _page_size f ->
   *       f [])
   *   | Some contract ->
   *     get_operations ~state ~refresh Mhelpers.transaction_opt "Transaction"
   *       (fun ops ->
   *           let ops = List.filter (fun {bo_op; _} -> bo_op.mo_det.trd_dst = contract) ops in
   *           let n = List.length ops in
   *           update ?title ?suf_id ?page_sizer ?state:(Some state) n (fun page page_size f ->
   *               let start, length = page * page_size, page_size in
   *               let ops = Misc.sublist ~start length ops in
   *               f ops)) *)

end

module History = struct
  let use_dunscan fwith fwithout f =
    Storage_reader.get_config
      (function {use_dunscan = true; _} -> fwith f | _ -> fwithout f)

  let get_operations_page ?error ?page ?page_size ?manager ~state ~refresh filter kind f =
    use_dunscan
      (Scan.get_operations_page ?error ?page ?page_size ?manager ~state ~refresh filter kind)
      (Storage.get_operations_page ?page ?page_size ?manager ~state ~refresh filter kind)
      f

  let page_wrap ?error ?title ?suf_id ?page_sizer ~state ~refresh
      filter kind update =
    use_dunscan
      (Scan.page_wrap ?error ?title ?suf_id ?page_sizer ~state ~refresh
         filter kind)
      (Storage.page_wrap ?title ?suf_id ?page_sizer ~state ~refresh filter kind)
      update

  let page_wrap_simple ?error ?page ?page_size ~state ~refresh filter kind update =
    use_dunscan
      (Scan.page_wrap_simple ?error ?page ?page_size ~state ~refresh filter kind)
      (Storage.page_wrap_simple ?page ?page_size ~state ~refresh filter kind)
      update

  (* let get_notarizations ?error ?title ?suf_id ?page_sizer ~state ~refresh
   *     update =
   *   use_dunscan
   *     (Scan.get_notarizations ?error ?title ?suf_id ?page_sizer ~state ~refresh)
   *     (Storage.get_notarizations ?title ?suf_id ?page_sizer ~state ~refresh)
   *     update *)

  let get_activated ?error ~state f =
    use_dunscan
      (Scan.get_activated ?error ~state)
      (fun f -> f false) (* will try to activate anyway *)
      f

  let get_services ?error ~state f =
    use_dunscan
      (Scan.get_services ?error ~state)
      (fun f -> f []) (* no services without dunscan *)
      f

  let update_pending ?error ~state f =
    use_dunscan
      (Scan.update_pending ?error ~state)
      (fun _ -> ())
      f
end
