(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

include MMisc

module MLwt = struct
  let (>>=) = Lwt.(>>=)
  let (>|=) = Lwt.(>|=)
  let return = Lwt.return
  let return_unit = Lwt.return_unit
  let return_ok = Lwt.return_ok
  let return_error = Lwt.return_error
  let async = Lwt.async

  let wrap_err f = function
    | Error e -> return_error e
    | Ok x -> f x

  let wrap_err_end f = function
    | Error e -> return_error e
    | Ok x -> return_ok @@ f x

  (* ('a, 'b) result lwt -> ('a -> ('c, 'b) result lwt) -> ('c * 'b) result lwt *)
  let (>>=?) v f = v >>= (wrap_err f)

  (* ('a, 'b) result lwt -> ('a -> 'c) -> ('c * 'b) result lwt *)
  let (>>|?) v f = v >>= (wrap_err_end f)

  let async_req ?error req update =
    async (fun () -> req >>= function
      | Error e -> (match error with
          | None -> return_unit
          | Some error -> let code, content = error_content e in
            return (error code content))
      | Ok res -> Lwt.return (update res))

  let async_req_opt ?error ?callback req =
    async_req ?error req (match callback with None -> (fun _ -> ()) | Some callback -> callback)

  let async_exn p update =
    async (fun () -> p >>= function
      | Error e -> let (code, content) = error_content0 e in
        Printf.eprintf "Error in async_exn %d %s\n%!" code content;
        return_unit
      | Ok x -> update x; return_unit)

  let async_exn_opt ?callback p =
    async_exn p (match callback with None -> (fun _ -> ()) | Some callback -> callback)

  let async0 p update =
    async (fun () -> p >>= fun x -> update x; return_unit)

  let async0_opt ?callback p =
    async0 p (match callback with None -> (fun _ -> ()) | Some callback -> callback)

  let map_res_s f l =
    Lwt_list.fold_left_s (fun acc x -> match acc with
        | Error e -> return_error e
        | Ok acc -> f x >>|? fun r -> r :: acc) (Ok []) l >>|? fun l ->
    List.rev l

  let mapi_res_s f l =
    Lwt_list.fold_left_s (fun (i, acc) x -> match acc with
        | Error e -> return (i+1, Error e)
        | Ok acc -> f i x >>= function
          | Error e -> return (i+1, Error e)
          | Ok r -> return (i+1, Ok (r :: acc))) (0, Ok []) l >|= snd >>|?
    List.rev
end

include MLwt
