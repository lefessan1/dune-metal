module Dunscan = Data_types_min
module Metal = Metal_types
open Dunscan

let convopt f = function
  | None -> None
  | Some x -> Some (f x)

let dunscan_errors = function
  | None -> None
  | Some l -> Some (List.map (fun {err_kind; err_id; err_info} ->
      {Metal.err_kind; err_id; err_info}) l)

let dunscan_params = function
  | None -> None
  | Some {p_entrypoint; p_value} ->
    let value =
      convopt (EzEncoding.construct (
          Api_encoding_min.V1.Op.value_encoding false)) p_value in
    Some (p_entrypoint, value)

let dunscan_script = function
  | None -> None
  | Some {sc_code; sc_storage; sc_code_hash} -> Some (sc_code, sc_storage, sc_code_hash)

let dunscan_delegate s = if s = "" then None else Some s

let dunscan_mo = function
  | Transaction tr -> {
      Metal.mi_source = tr.tr_src.dn;
      mi_fee = Some tr.tr_fee;
      mi_counter = Some (Int32.to_int tr.tr_counter);
      mi_gas_limit = Some tr.tr_gas_limit;
      mi_storage_limit = Some tr.tr_storage_limit;
      mi_burn = Some tr.tr_burn;
      mi_errors = dunscan_errors tr.tr_errors;
      mi_failed = tr.tr_failed;
      mi_internal = tr.tr_internal },
      Metal.TraDetails {
        Metal.trd_dst = tr.tr_dst.dn;
        trd_amount = tr.tr_amount;
        trd_parameters = dunscan_params tr.tr_parameters;
        trd_collect_call = match tr.tr_collect_fee_gas with
          | None -> false
          | Some _fee -> true
      },
      tr.tr_timestamp, tr.tr_op_level
  | Origination ori -> {
      Metal.mi_source = ori.or_src.dn;
      mi_fee = Some ori.or_fee;
      mi_counter = Some (Int32.to_int ori.or_counter);
      mi_gas_limit = Some ori.or_gas_limit;
      mi_storage_limit = Some ori.or_storage_limit;
      mi_burn = Some ori.or_burn;
      mi_errors = dunscan_errors ori.or_errors;
      mi_failed = ori.or_failed;
      mi_internal = ori.or_internal},
      Metal.OriDetails {
        Metal.ord_balance = ori.or_balance;
        ord_kt1 = Some ori.or_kt1.dn;
        ord_delegate = dunscan_delegate ori.or_delegate.dn;
        ord_script = dunscan_script ori.or_script },
      ori.or_timestamp, ori.or_op_level
  | Delegation del -> {
         Metal.mi_source = del.del_src.dn;
         mi_fee = Some del.del_fee;
         mi_counter = Some (Int32.to_int del.del_counter);
         mi_gas_limit = Some del.del_gas_limit;
         mi_storage_limit = Some del.del_storage_limit;
         mi_burn = None;
         mi_errors = dunscan_errors del.del_errors;
         mi_failed = del.del_failed;
         mi_internal = del.del_internal},
    Metal.DelDetails (dunscan_delegate del.del_delegate.dn),
    del.del_timestamp, del.del_op_level
  | Reveal rvl -> {
         Metal.mi_source = rvl.rvl_src.dn;
         mi_fee = Some rvl.rvl_fee;
         mi_counter = Some (Int32.to_int rvl.rvl_counter);
         mi_gas_limit = Some rvl.rvl_gas_limit;
         mi_storage_limit = Some rvl.rvl_storage_limit;
         mi_burn = None;
         mi_errors = dunscan_errors rvl.rvl_errors;
         mi_failed = rvl.rvl_failed;
         mi_internal = rvl.rvl_internal},
    Metal.RvlDetails rvl.rvl_pubkey,
    rvl.rvl_timestamp, rvl.rvl_op_level

let dunscan_mo_dune = function
  | Manage_account mac ->
    let target = MMisc.convopt (fun ({dn; _}, _) -> dn) mac.mac_target in
    Some ({
      Metal.mi_source = mac.mac_src.dn;
      mi_fee = Some mac.mac_fee;
      mi_counter = Some (Int32.to_int mac.mac_counter);
      mi_gas_limit = Some mac.mac_gas_limit;
      mi_storage_limit = Some mac.mac_storage_limit;
      mi_burn = None;
      mi_errors = dunscan_errors mac.mac_errors;
      mi_failed = mac.mac_failed;
      mi_internal = mac.mac_internal},
      Metal.ManDetails (target, Metal.MadDecoded {
            Metal.mao_maxrolls = mac.mac_maxrolls;
            mao_admin = convopt (convopt (fun {dn; _} -> dn)) mac.mac_admin;
            mao_white_list = convopt (List.map (fun {dn; _} -> dn)) mac.mac_white_list;
            mao_delegation = mac.mac_delegation;
            mao_recovery = convopt (convopt (fun {dn; _} -> dn)) mac.mac_recovery;
            mao_actions = match mac.mac_actions with None -> [] | Some l -> l;
          }),
        mac.mac_timestamp, mac.mac_op_level)
  | _ -> None

let dunscan_op {op_hash; op_block_hash; op_network_hash; op_type} =
  let bo_op, bi_timestamp, bi_level = match op_type with
    | Anonymous _ | Tokened _ -> [], "", -1
    | Sourced s -> match s with
      | Endorsement _ | Amendment _ | Dictator _ -> [], "", -1
      | Manager (_, _, l) ->
        let ops, tsp, level = List.fold_left (
            fun (acc_ops, acc_tsp, acc_level) op ->
              let mo_info, mo_det, tsp, level = dunscan_mo op in
              let acc_tsp, acc_level =
                if mo_info.Metal.mi_internal then acc_tsp, acc_level
                else if acc_tsp = "" then tsp, level
                else acc_tsp, acc_level in
              Metal.{mo_info; mo_det} :: acc_ops, acc_tsp, acc_level)
            ([], "", -1) l in
        List.rev ops, tsp, level
      | Dune_manager (_, l) ->
        let ops, tsp, level = List.fold_left (
            fun (acc_ops, acc_tsp, acc_level) op ->
              match dunscan_mo_dune op with
              | None -> acc_ops, acc_tsp, acc_level
              | Some (mo_info, mo_det, tsp, level) ->
                let acc_tsp, acc_level =
                  if mo_info.Metal.mi_internal then acc_tsp, acc_level
                  else if acc_tsp = "" then tsp, level
                  else acc_tsp, acc_level in
                Metal.{mo_info; mo_det} :: acc_ops, acc_tsp, acc_level)
            ([], "", -1) l in
        List.rev ops, tsp, level in
  Metal.{
    bo_hash = op_hash;
    bo_block = Some {
        bi_protocol = "";
        bi_network = op_network_hash;
        bi_hash = op_block_hash;
        bi_timestamp;
        bi_level
      };
    bo_branch = None;
    bo_op }

let dunscan_ops l =
  List.rev @@ List.fold_left (fun acc o ->
      let bo = dunscan_op o in
      match bo.Metal.bo_op with [] -> acc | _ -> bo :: acc) [] l
