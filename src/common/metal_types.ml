(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

type block_hash = string
type operation_hash = string
type account_hash = string
type protocol_hash = string
type network_hash = string
type timestamp = string

type account_name = { dn : account_hash; alias : string option }

type network =
  | Mainnet | Testnet | Devnet
  | Custom of string

type metal_state = Disabled | Locked | Unlocked

type manager_operation_type =
    Transaction | Origination | Delegation | Reveal | Manage_account

type transaction_details = {
  trd_dst : account_hash;
  trd_amount : int64;
  trd_parameters : (string option * string option) option;
  trd_collect_call : bool;
}

type origination_details = {
  ord_balance : int64;
  ord_kt1 : account_hash option;
  ord_delegate : account_hash option;
  ord_script : (string option * string * string option) option;
}

type manage_account_decoded_options = {
  mao_maxrolls : int option option;
  mao_admin : account_hash option option;
  mao_white_list : account_hash list option;
  mao_delegation : bool option;
  mao_recovery : account_hash option option;
  mao_actions : (string * string) list;
}

type manage_account_options =
  | MadDecoded of manage_account_decoded_options
  | MadJSON of string

type 'a manager_details =
  | TraDetails of transaction_details
  | OriDetails of origination_details
  | DelDetails of account_hash option
  | RvlDetails of string
  | ManDetails of ('a * manage_account_options)

type op_error = {
  err_kind: string;
  err_id: string;
  err_info: string;
}

type manager_info = {
  mi_source : account_hash;
  mi_fee : int64 option;
  mi_counter : int option;
  mi_gas_limit : Z.t option;
  mi_storage_limit : Z.t option;
  mi_burn : int64 option;
  mi_errors : op_error list option;
  mi_failed : bool;
  mi_internal : bool;
}

type ('a, 'b) manager_operation = {
  mo_info : 'a;
  mo_det : 'b;
}

type block_info = {
  bi_protocol : protocol_hash;
  bi_network : network_hash;
  bi_hash : block_hash;
  bi_timestamp : timestamp;
  bi_level : int;
}

type 'a block_operation = {
  bo_hash : operation_hash;
  bo_block : block_info option;
  bo_branch : block_hash option;
  bo_op : 'a
}

type notif_manager_info = {
  not_mi_fee : int64 option;
  not_mi_gas_limit : Z.t option;
  not_mi_storage_limit : Z.t option;
}

type 'a notif_generic = {
  not_origin : string;
  not_tsp : string;
  not_id : string;
  not_wid : int;
  not_network : network option;
  not_type : string option;
  not_op : 'a
}

type notif_op =
  (notif_manager_info, string option manager_details) manager_operation notif_generic

type notif_batch =
  (notif_manager_info, string option manager_details) manager_operation list notif_generic

type notif_app = {
  not_approv_id : string ;
  not_approv_wid : int ;
  not_approv_tsp : string ;
  not_approv_icon : string option ;
  not_approv_name : string ;
  not_approv_url : string ;
}

type notif_kind =
  | OpNot of notif_op
  | BatchNot of notif_batch
  | ApprovNot of notif_app

type vault =
  | Ledger of (string * Bigstring.t)
  | Local of (Bigstring.t * Bigstring.t)

type 'a network_assoc = (network * 'a) list

type account = {
  acc_index : int;
  pkh : account_hash;
  manager : account_hash;
  vault : vault;
  name : string;
  revealed : operation_hash network_assoc;
  pending : (manager_info, string option manager_details) manager_operation list block_operation list network_assoc;
  history : (manager_info, string option manager_details) manager_operation list block_operation list network_assoc;
  manager_kt : bool option;
  admin : account_hash network_assoc;
}

type local_notif = {
  notif_acc  : account_hash ;
  notifs : notif_kind list ;
}

type faucet = {
  fc_mnemonic : string list;
  fc_code : string;
  fc_pkh: account_hash;
  fc_password : string;
  fc_email : string;
  fc_amount : int64 option
}

type service = {
  srv_kind : string;
  srv_dn1 : string option;
  srv_name : string;
  srv_url : string;
  srv_logo : string;
  srv_logo2 : string option;
  srv_logo_payout : string option;
  srv_descr : string option;
  srv_sponsored : string option;
  srv_page : string option;
  srv_delegations_page : bool ;
  srv_account_page : bool ;
  srv_aliases: account_name list option ;
  srv_display_delegation_page : bool ;
  srv_display_account_page : bool ;
}

type state = {
  fo_acc : account;
  fo_net : network;
}
type full_state = {
  ho_acc : account;
  ho_net : network;
  ho_accs : account list
}
type se_state = {
  se_acc : account option;
  se_accs : account list
}

type entry_ty =
  | EAUnit
  | EABytes
  | EAString
  | EANat
  | EAInt
  | EABool
  | EATimestamp
  | EAMutez
  | EAAddress
  | EAOperation
  | EAKey
  | EAKey_hash
  | EASignature
  | EAChain_id
  | EAContract
  | EAList of entry_arg
  | EAPair of entry_arg * entry_arg
  | EAOption of entry_arg
  | EAOr of entry_arg * entry_arg
  | EASet of entry_arg
  | EAMap of entry_arg * entry_arg
  | EABigmap of entry_arg

and entry_arg = {
  ea_name : string option ;
  ea_type : string option ;
  ea_arg : entry_ty ;
}

type entry_point = {
  ep_name : string option;
  ep_type : string option ;
  ep_args : entry_arg ;
  ep_mic : string list ;
}

type config = {
  use_dunscan : bool;
  theme : string;
  batch_notif : bool;
  notif_timeout : float;
}

type 'a forge_output = {
  fg_bytes : Bigstring.t;
  fg_ops : 'a list;
  fg_branch : string;
  fg_protocol : string;
}
