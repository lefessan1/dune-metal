open Micheline_types
open Json_encoding

let z = conv Z.to_string Z.of_string string
let hex = conv (fun (`Hex h) -> h) (fun h -> `Hex h) string
let bytes = conv Hex.of_bytes Hex.to_bytes hex
let uint16 = int

let canonical_location_encoding =
  def
    "micheline.location"
    ~title:
      "Canonical location in a Micheline expression"
    ~description:
      "The location of a node in a Micheline expression tree \
       in prefix order, with zero being the root and adding one \
       for every basic node, sequence and primitive application." @@
  int

let internal_canonical_encoding ~variant prim_encoding =
  let int_encoding =
    obj1 (req "int" z) in
  let string_encoding =
    obj1 (req "string" string) in
  let bytes_encoding =
    obj1 (req "bytes" bytes) in
  let int_encoding =
    case int_encoding
      (function Int (_, v) -> Some v | _ -> None)
      (fun v -> Int (0, v)) in
  let string_encoding =
    case string_encoding
      (function String (_, v) -> Some v | _ -> None)
      (fun v -> String (0, v)) in
  let bytes_encoding =
    case bytes_encoding
      (function Bytes (_, v) -> Some v | _ -> None)
      (fun v -> Bytes (0, v)) in
  let seq_encoding expr_encoding =
    case (list expr_encoding)
      (function Seq (_, v) -> Some v | _ -> None)
      (fun args -> Seq (0, args)) in
  let annots_encoding = list string in
  let application_encoding expr_encoding =
    case
      (obj3 (req "prim" prim_encoding)
         (dft "args" (list expr_encoding) [])
         (dft "annots" annots_encoding []))
      (function Prim (_, prim, args, annots) -> Some (prim, args, annots)
              | _ -> None)
      (fun (prim, args, annots) -> Prim (0, prim, args, annots)) in
  let node_encoding = mu ("micheline." ^ variant ^ ".expression") (fun expr_encoding ->
      union
        [ int_encoding ;
          string_encoding ;
          bytes_encoding ;
          seq_encoding expr_encoding ;
          application_encoding expr_encoding ])
  in
  conv
    (function Canonical node -> node)
    (fun node -> strip_locations node)
    node_encoding

let canonical_encoding ~variant prim_encoding =
  internal_canonical_encoding ~variant prim_encoding

let table_encoding ~variant location_encoding prim_encoding =
  conv
    (fun node ->
       let canon, assoc = extract_locations node in
       let _, table = List.split assoc in
       (canon, table))
    (fun (canon, table) ->
       let table = Array.of_list table in
       inject_locations (fun i -> table.(i)) canon)
    (obj2
       (req "expression" (canonical_encoding ~variant prim_encoding))
       (req "locations" (list location_encoding)))

let erased_encoding ~variant default_location prim_encoding =
  conv
    (fun node -> strip_locations node)
    (fun canon -> inject_locations (fun _ -> default_location) canon)
    (canonical_encoding ~variant prim_encoding)
