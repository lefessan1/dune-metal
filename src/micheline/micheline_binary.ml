open Binary_writer

let primitives =
  [ "parameter";
    "storage";
    "code";
    "False";
    "Elt";
    "Left";
    "None";
    "Pair";
    "Right";
    "Some";
    "True";
    "Unit";
    "PACK";
    "UNPACK";
    "BLAKE2B";
    "SHA256";
    "SHA512";
    "ABS";
    "ADD";
    "AMOUNT";
    "AND";
    "BALANCE";
    "CAR";
    "CDR";
    "CHECK_SIGNATURE";
    "COMPARE";
    "CONCAT";
    "CONS";
    "CREATE_ACCOUNT";
    "CREATE_CONTRACT";
    "IMPLICIT_ACCOUNT";
    "DIP";
    "DROP";
    "DUP";
    "EDIV";
    "EMPTY_MAP";
    "EMPTY_SET";
    "EQ";
    "EXEC";
    "FAILWITH";
    "GE";
    "GET";
    "GT";
    "HASH_KEY";
    "IF";
    "IF_CONS";
    "IF_LEFT";
    "IF_NONE";
    "INT";
    "LAMBDA";
    "LE";
    "LEFT";
    "LOOP";
    "LSL";
    "LSR";
    "LT";
    "MAP";
    "MEM";
    "MUL";
    "NEG";
    "NEQ";
    "NIL";
    "NONE";
    "NOT";
    "NOW";
    "OR";
    "PAIR";
    "PUSH";
    "RIGHT";
    "SIZE";
    "SOME";
    "SOURCE";
    "SENDER";
    "SELF";
    "STEPS_TO_QUOTA";
    "SUB";
    "SWAP";
    "TRANSFER_TOKENS";
    "SET_DELEGATE";
    "UNIT";
    "UPDATE";
    "XOR";
    "ITER";
    "LOOP_LEFT";
    "ADDRESS";
    "CONTRACT";
    "ISNAT";
    "CAST";
    "RENAME";
    "bool";
    "contract";
    "int";
    "key";
    "key_hash";
    "lambda";
    "list";
    "map";
    "big_map";
    "nat";
    "option";
    "or";
    "pair";
    "set";
    "signature";
    "string";
    "bytes";
    "mutez";
    "timestamp";
    "unit";
    "operation";
    "address";
    "SLICE";
    (* Dune specific *)
    "GETBAL";
    "LEVEL";
    "ISIMP";
    "COLLCALL";
    (* 005 specific *)
    "DIG";
    "DUG";
    "EMPTY_BIG_MAP";
    "APPLY";
    "chain_id";
    "CHAIN_ID";
    (* dune 005 addition *)
    "GET_ACCOUNT_INFO";
    "MANAGE_ACCOUNT";
    "ADMIN_ACCOUNTS";
    "ADMIN_PROTOCOL";
    "ALLOW_PEERS";
    "REVOKE_PEERS";
  ]

let forge_primitive p =
  match List.fold_left (fun (i, acc) s ->
      if acc = None && s = p then (i, Some i) else (i+1, acc)) (0, None) primitives with
  | _, None -> uint8 0;
  | _, Some i -> uint8 i

let forge_annot l =
  elem @@ Bigstring.concat " " (List.map Bigstring.of_string l)

let forge_micheline = function
  | Micheline_types.Canonical sc ->
    let rec aux = function
      | Micheline_types.Int (_, z) -> list [ uint8 0; zarith z ]
      | Micheline_types.String (_, s) -> list [ uint8 1; elem @@ Bigstring.of_string s ]
      | Micheline_types.Bytes (_, h) -> list [ uint8 10; elem @@ Bigstring.of_bytes h ]
      | Micheline_types.Seq (_, l) ->
        list [ uint8 2; elem (list @@ List.map aux l) ]
      | Micheline_types.Prim (_, s, l, an) ->
        let prm = forge_primitive s in match l, an with
        | [], [] -> list [ uint8 3; prm ]
        | [], an -> list [ uint8 4; prm; forge_annot an ]
        | [ arg1 ], [] -> list [ uint8 5; prm; aux arg1 ]
        | [ arg1 ], an ->
          list [ uint8 6; prm; aux arg1; forge_annot an ]
        | [ arg1; arg2 ], [] ->
          list [ uint8 7; prm; aux arg1; aux arg2 ]
        | [ arg1; arg2 ], an ->
          list [ uint8 8; prm; aux arg1; aux arg2;
                 forge_annot an ]
        | args, an ->
          list [ uint8 9; prm; elem @@ list @@ List.map aux args;
                 forge_annot an ] in
    aux sc
