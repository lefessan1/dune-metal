(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min

let unlock_account app =
  let account = Storage_utils.Of_js.account app##.unlock##.account in
  let password = to_string app##.unlock##.password in
  Vault.Cb.unlock ~error:(fun _ _ -> app##.unlock##.pwd_state_ := some _false)
    ~callback:(fun _ ->
        app##.unlock##.password := string "";
        let data = object%js val msg = string "metal unlocked" end in
        Notif_background.send ~data ();
        app##.unlock##.callback app)
    account password

let make ?callback app =
  Storage_reader.get_account_js @@ fun account _accs ->
  match Optdef.to_option account with
  | None -> Metal_start.make app
  | Some account ->
    (match callback with Some cb -> app##.unlock##.callback := cb | _ -> ());
    app##.unlock##.account := account;
    app##.path := string "unlock"

let init () =
  V.add_method0 "unlock_account" unlock_account;
  object%js
    val mutable callback = fun _ -> ()
    val mutable account = Dummy.account ()
    val mutable password = string ""
    val mutable pwd_state_ = null
  end
