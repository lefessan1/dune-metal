(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types
open Storage_utils
open Ezjs_min

let (>>=) = MLwt.(>>=)
let (>>=?) = MLwt.(>>=?)

(* Global config *)
let update_config app config =
  app##.use_dunscan_ := bool config.use_dunscan;
  app##.theme := string config.theme;
  app##.batch_notif_ := bool config.batch_notif;
  app##.notif_timeout_ := number_of_float (config.notif_timeout /. 1000.)

let set_config app =
  Mui.wait @@ fun () ->
  let use_dunscan = to_bool app##.settings##.use_dunscan_ in
  let theme = to_string app##.settings##.theme in
  let batch_notif = to_bool app##.settings##.batch_notif_ in
  let notif_timeout = 1000. *. float_of_number app##.settings##.notif_timeout_ in
  Storage_writer.update_config ~callback:(fun _ ->
      Theme.change_theme app theme) {use_dunscan; theme; batch_notif; notif_timeout}

(* Accounts config *)
let update_settings_accounts app accounts =
  app##.accounts := To_js.accounts accounts

let switch_accounts app i j =
  let accounts = to_array app##.accounts in
  let acc_i, acc_j = accounts.(i), accounts.(j) in
  accounts.(i) <- acc_j; accounts.(j) <- acc_i;
  app##.accounts := array accounts

let save_accounts app =
  Storage_writer.update_accounts_js ~remove:(to_list app##.remove) app##.accounts

let remove_account app pkh_to_remove =
  let accounts = Of_js.accounts app##.accounts in
  let index = List.fold_left
      (fun i acc -> if acc.pkh = to_string pkh_to_remove then acc.acc_index else i)
      (-1) accounts in
  let accounts = List.filter (fun {pkh; _} -> pkh <> to_string pkh_to_remove) accounts in
  app##.remove := of_list (index :: to_list app##.remove);
  update_settings_accounts app accounts

(* Password config *)
let reencrypt_accounts ?error old_pwd new_pwd accounts f =
  let rec iter old_accounts new_accounts = match old_accounts, new_accounts with
    | [], new_accounts -> MLwt.return (Ok (List.rev new_accounts))
    | {pkh; vault; _} :: old_accounts, new_accounts ->
      match vault with
      | Local vault ->
        Vault.Lwt.decrypt_base ~password:old_pwd ~pkh vault >>=? fun sk ->
        Vault.Lwt.encrypt_base ~password:new_pwd ~pkh sk >>=? fun new_account ->
        iter old_accounts (new_account :: new_accounts)
      | Ledger (path, _) ->
        Vault.Lwt.hash_password ~pwd:new_pwd ~pkh path >>=? fun new_account ->
        iter old_accounts (new_account :: new_accounts) in
  MLwt.async_req ?error (iter accounts []) f

let change_pwd app =
  let old_pwd = to_string app##.old_pwd_ in
  let new_pwd = to_string app##.new_pwd_ in
  let confirm_pwd = to_string app##.confirm_pwd_ in
  if new_pwd <> confirm_pwd then app##.confirm_pwd_state_ := some _false
  else if String.length new_pwd < 8 then app##.new_pwd_state_ := some _false
  else
    Vault.Cb.check_password old_pwd @@ function
    | false -> app##.old_pwd_state_ := some _false
    | true ->
      let accounts = Of_js.accounts app##.accounts in
      reencrypt_accounts old_pwd new_pwd accounts @@ fun accs ->
      Vault.Cb.set_password ~callback:(fun _ ->
          Storage_writer.update_accounts
            ~callback:(fun () -> update_settings_accounts app accs)
            accs)
        (Some new_pwd)

(* Secret key recover *)
let show_secret_key app =
  let a_pkh = to_string app##.account_secret_ in
  let pwd = to_string app##.pwd_secret_ in
  let accounts = to_listf Of_js.account app##.accounts in
  let account = List.find (fun {pkh; _} -> a_pkh = pkh) accounts in
  Vault.Cb.password @@ fun password ->
  if Some pwd <> password then app##.pwd_secret_state_ := some _false
  else
    match account.vault with
    | Ledger (path, _) ->
      app##.secret_key_ := string path
    | Local vault ->
      Vault.Cb.decrypt_base ?password ~pkh:account.pkh vault @@ fun sk ->
      let sk_b58 = MCrypto.Sk.b58enc sk in
      app##.secret_key_ := string sk_b58

let update_accounts app accounts =
  app##.download_href_ := undefined;
  app##.export_state_ := null;
  app##.accounts := accounts;
  let tmp_accounts = to_listf (fun x -> x##.pkh, to_string x##.vault##.kind) accounts in
  let tmp_accounts = List.filter (fun (_pkh, vault) -> vault = "local") tmp_accounts in
  app##.accounts_secret_ := of_listf fst tmp_accounts

(* Custom networks config *)
let remove_network app name =
  Storage_writer.remove_custom_network
    ~callback:(fun l -> app##.networks := To_js.custom_networks l)
    ~networks:(Of_js.custom_networks app##.networks) (to_string name)

let add_network app =
  let node = to_string app##.network_node_ in
  let api = to_string app##.network_scan_ in
  let name = to_string app##.network_name_ in
  if node = "" then app##.network_node_state_ := some _false
  else if api = "" then app##.network_scan_state_ := some _false
  else if name = "" then app##.network_name_state_ := some _false
  else
    Storage_writer.add_custom_network ~networks:(Of_js.custom_networks app##.networks)
      ~callback:(fun l -> app##.networks := To_js.custom_networks l) name node api

let update_networks app networks =
  app##.download_href_ := undefined;
  app##.export_state_ := null;
  app##.networks := networks

(* Import / Export *)

let export_data app =
  let pwd = to_string app##.export_password_ in
  app##.export_password_ := string "";
  let rec aux f acc = function
    | [] -> f (List.rev acc)
    | h :: t -> match h.vault with
      | Ledger _ -> aux f acc t
      | Local vault ->
        Vault.Cb.decrypt_base ~password:pwd ~pkh:h.pkh vault (fun sk ->
            let sk_b58 = MCrypto.Sk.b58enc sk in
            aux f ((h.name, h.pkh, sk_b58, h.manager, h.manager_kt) :: acc) t) in
  Vault.Cb.check_password pwd @@ function
  | false -> app##.export_state_ := some _false
  | true ->
    let accounts = to_listf Of_js.account app##.accounts in
    let networks = to_listf Of_js.custom_network app##.networks in
    let f accounts =
      let s = EzEncoding.construct ~compact:false
          Metal_encoding.export_encoding (accounts, networks) in
      app##.download_href_ := def (string ("data:text/plain;charset=utf-8," ^ s));
      app##.export_state_ := some _true in
    aux f [] accounts

let load_data app =
  Mui.wait @@ fun () ->
  match Opt.to_option app##.import_file_ with
  | None -> app##.export_state_ := some _false
  | Some f -> match Opt.to_option @@ File.CoerceTo.blob f with
    | None -> app##.export_state_ := some _false
    | Some f ->
      Mui.read_file f @@ fun s ->
      app##.import_string_ := def (string s)

let import_data app =
  let rec aux ~password f (acc, acc_index) = function
    | [] -> f (List.rev acc)
    | (name, pkh, sk, manager, manager_kt) :: t ->
      match List.find_opt (fun a -> a.name = name) acc with
      | Some _ -> aux ~password f (acc, acc_index) t
      | None ->
        let sk_b = MCrypto.Sk.b58dec sk in
        Vault.Cb.encrypt_base ~error:(fun _ _ -> app##.export_state_ := some _false)
          ~password ~pkh ~manager sk_b (fun new_account ->
              aux ~password f ({new_account with name; acc_index; manager_kt} :: acc,
                               acc_index+1) t) in
  Vault.Cb.password @@ function
  | None -> app##.import_state_ := some _false
  | Some password ->
    match to_optdef to_string app##.import_string_ with
    | None -> app##.import_state_ := some _false
    | Some s ->
      let (accounts_new, networks) =
        EzEncoding.destruct Metal_encoding.export_encoding s in
      let f accs =
        Storage_writer.update_custom_networks ~callback:(fun ns ->
            update_networks app (To_js.custom_networks ns)) networks;
        Storage_writer.update_accounts ~callback:(fun () ->
            update_accounts app (To_js.accounts accs)) accs;
        app##.import_state_ := some _true;
        app##.import_file_ := null in
      let accounts = Of_js.accounts app##.accounts in
      let max_index = List.fold_left (fun ind acc -> max acc.acc_index ind) 0 accounts in
      aux ~password f (accounts, max_index + 1) accounts_new

let clear_local app =
  Storage_writer.clear_local ();
  app##.local_cleared_ := _true

let make_settings app =
  Storage_reader.get_config (update_config app##.settings);
  Storage_reader.get_accounts_js (update_accounts app##.settings);
  Storage_reader.get_custom_networks_js (update_networks app##.settings);
  app##.path := string "settings"

let make app =
  Storage_reader.is_enabled @@ fun enabled ->
  if not enabled then Metal_start.make app
  else
    Storage_reader.is_unlocked @@ fun unlocked ->
    if not unlocked then Metal_unlock.make ~callback:make_settings app
    else make_settings app

let init () =
  let settings f app = f app##.settings in
  V.add_method0 "set_config" set_config;
  V.add_method0 "show_secret_key" (settings show_secret_key);
  V.add_method0 "change_pwd" (settings change_pwd);
  V.add_method1 "remove_account" (settings remove_account);
  V.add_method0 "save_accounts" (settings save_accounts);
  V.add_method "switch_accounts" (settings switch_accounts);
  V.add_method0 "add_network" (settings add_network);
  V.add_method1 "remove_network" (settings remove_network);
  V.add_method0 "export_data" (settings export_data);
  V.add_method0 "load_data" (settings load_data);
  V.add_method0 "import_data" (settings import_data);
  V.add_method0 "clear_local" (settings clear_local);
  Dummy.settings ()
