(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Chrome

let refuse_notif_popup app =
  app##.path := string "home";
  MCore.refuse_notif app

let inject_op_popup app =
  MCore.inject_op app

let approve_url_popup app =
  app##.path := string "home";
  MCore.approve_url app

let close_notif app =
  app##.path := string "home";
  MCore.clear_notif app

let create_tab url =
  Tabs.(create @@ make_create ~url ()) ;
  Dom_html.window##close

let make_qrcode app =
  Mui.wait @@ fun () ->
  ignore @@ Ezjs_qrcode.make_code "qr-code" (to_string app##.core##.account##.pkh)

let clear_import_popup app =
  app##.path := string "home"

let add_to_batch_popup app =
  app##.path := string "home";
  MCore.add_to_batch app

let postpone_batch_popup app =
  app##.path := string "home";
  MCore.postpone_batch app

let route app s_js =
  app##.path := string "loading";
  let s = to_string s_js in
  log "route %S" s;
  match s with
  | "home" -> MCore.make app
  | "unlock" -> Metal_unlock.make ~callback:MCore.make app
  | "lock" -> MCore.lock app
  | "refresh" ->
    MCore.update_account app##.core;
    app##.path := string "home";
  | "settings" -> create_tab "index.html?path=settings"
  | "expand" -> create_tab "index.html?path=home"
  | "presentation" -> create_tab "index.html?path=presentation"
  | "trexp" ->
    let tr_args =
      match Optdef.to_option app##.core##.op##.transaction with
      | Some tr ->
        Printf.sprintf
          "&tr_dst=%s&tr_am=%s"
          (to_string tr##.destination)
          (to_string tr##.amount)
      | None -> "" in
    create_tab ("index.html?path=home&type=transaction&expanded=true" ^ tr_args)
  | "delexp" ->
    let del_args =
      match Optdef.to_option app##.core##.op##.delegation with
      | Some del ->
        begin match Optdef.to_option del##.delegate with
          | None -> ""
          | Some dlg -> Printf.sprintf "&dlg_delegate=%s" (to_string dlg)
        end
      | None -> "" in
    create_tab ("index.html?path=home&type=delegation&expanded=true" ^ del_args)
  | "reset" -> create_tab "index.html?path=reset"
  | "new" -> create_tab "index.html?path=start"
  | "deposit" ->
    app##.path := s_js;
    make_qrcode app
  | "import" ->
    MCore.get_import_accounts app##.core;
    app##.path := s_js
  | _ -> app##.path := s_js

let () =
  Runtime.onConnect (fun _port -> ());
  V.add_method1 "route" route;
  V.add_method0 "refuse_notif_popup" refuse_notif_popup;
  V.add_method0 "inject_op_popup" inject_op_popup;
  V.add_method0 "approve_url_popup" approve_url_popup;
  V.add_method0 "clear_import_popup" clear_import_popup;
  V.add_method0 "add_to_batch_popup" add_to_batch_popup;
  V.add_method0 "postpone_batch_popup" postpone_batch_popup;
  let unlock = Metal_unlock.init () in
  let core = MCore.init () in
  V.add_method0 "close_notif" close_notif;
  let path = "home" in
  let theme = Theme.light_theme in
  let app = V.init ~path ~unlock ~core ~theme ~render:Render.popup_render
      ~static_renders:Render.popup_static_renders () in
  route app (string path)
