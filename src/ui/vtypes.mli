open Ezjs_min
open Storage_utils

class type vue_modal = object
  method show : unit meth
  method hide : unit meth
end

class type error_js = object
  method code : int readonly_prop
  method content : js_string t optdef readonly_prop
end

class type pres_js = object
  method step : int prop
end

class type theme_js = object
  method bg_variant_ : js_string t opt readonly_prop
  method tx_variant_ : js_string t opt readonly_prop
  method bd_variant_ : js_string t opt readonly_prop
  method variant : js_string t opt readonly_prop
  method class_ : js_string t opt readonly_prop
  method border_class_ : js_string t opt readonly_prop
  method type_ : js_string t opt readonly_prop
  method outline : js_string t opt readonly_prop
  method dark : bool t readonly_prop
  method icon : js_string t readonly_prop
  method icon_variant_ : js_string t opt readonly_prop
end

class type vault = object
  method pkh : js_string t readonly_prop
  method sk : js_string t optdef readonly_prop
  method path : js_string t optdef readonly_prop
end

class type baker = object
  method name : js_string t readonly_prop
  method pkh : js_string t readonly_prop
  method logo : js_string t optdef readonly_prop
end

class type start_js = object
  method section : js_string t prop
  method account : account_js t optdef prop
  method vault : vault t prop
  method network : js_string t prop
  method networks : js_string t js_array t prop
  method file_ico_ : File.file_any opt prop
  method mnemonic_ico_ : js_string t prop
  method email_ico_ : js_string t prop
  method pwd_ico_ : js_string t prop
  method pkh_ico_ : js_string t prop
  method activation_ico_ : js_string t prop
  method json_ico_state_ : bool t opt prop
  method mnemonic_seed_ : js_string t prop
  method passphrase_seed_ : js_string t prop
  method mnemonic_seed_state_ : bool t opt prop
  method secret_key_ : js_string t prop
  method secret_key_pwd_ : js_string t prop
  method secret_key_decrypting_ : bool t prop
  method secret_key_state_ : bool t opt prop
  method hardware : js_string t prop
  method derivation_path_ : js_string t prop
  method mnemonic_new_ : js_string t prop
  method passphrase_new_ : js_string t prop
  method encrypt_pwd1_ : js_string t prop
  method encrypt_pwd2_ : js_string t prop
  method encrypt_pwd_ : js_string t prop
  method pwd1_state_ : bool t opt prop
  method pwd2_state_ : bool t opt prop
  method pwd_state_ : bool t opt prop
end

class type ['a] unlock_js = object
  method callback : ('a -> unit) prop
  method account : account_js t prop
  method password : js_string t prop
  method pwd_state_ : bool t opt prop
end

class type reset_js = object
  method confirmation : bool t prop
  method textarea_rows_ : int prop
  method accounts : js_string t prop
end

class type settings_js = object
  method use_dunscan_ : bool t prop
  method theme : js_string t prop
  method batch_notif_ : bool t prop
  method notif_timeout_ : number t prop
  method accounts : account_js t js_array t prop
  method accounts_secret_ : js_string t js_array t prop
  method account_secret_ : js_string t prop
  method pwd_secret_ : js_string t prop
  method secret_key_ : js_string t prop
  method account_secret_state_ : bool t opt prop
  method pwd_secret_state_ : bool t opt prop
  method old_pwd_ : js_string t prop
  method new_pwd_ : js_string t prop
  method confirm_pwd_ : js_string t prop
  method old_pwd_state_ : bool t opt prop
  method new_pwd_state_ : bool t opt prop
  method confirm_pwd_state_ : bool t opt prop
  method networks : custom_network t js_array t prop
  method network_node_ : js_string t prop
  method network_scan_ : js_string t prop
  method network_name_ : js_string t prop
  method network_node_state_ : bool t opt prop
  method network_scan_state_ : bool t opt prop
  method network_name_state_ : bool t opt prop
  method remove : int js_array t prop
  method export_password_ : js_string t prop
  method export_state_ : bool t opt prop
  method download_href_ : js_string t optdef prop
  method import_state_ : bool t opt prop
  method import_file_ : File.file_any opt prop
  method import_string_ : js_string t optdef prop
  method local_cleared_ : bool t prop
end

class type account_info = object
  method maxrolls : int optdef readonly_prop
  method admin : js_string t optdef readonly_prop
  method whitelist : js_string t js_array t optdef readonly_prop
  method delegation : bool t optdef readonly_prop
  method recovery : js_string t optdef readonly_prop
end

class type op_result = object
  method amount : js_string t readonly_prop
  method fee : js_string t readonly_prop
  method burn : js_string t readonly_prop
  method gas_limit_ : js_string t readonly_prop
  method storage_limit_ : js_string t readonly_prop
end

class type history_js = object
  method ops : block_operation_js t js_array t prop
  method page : int prop
  method total : int prop
end

class type notarization_info_js = object
  method file_hash_ : js_string t readonly_prop
  method file_name_ : js_string t readonly_prop
  method comments : js_string t optdef readonly_prop
end

class type core_js = object
  method network : js_string t prop
  method networks : js_string t js_array t prop
  method account : account_full_js t prop
  method accounts : account_full_js t js_array t prop
  method balance : js_string t optdef prop
  method account_info_ : account_info t optdef prop
  method notifications : notif_js t js_array t prop
  method import_accounts_ : js_string t js_array t prop
  method selected_import_accounts_ : js_string t js_array t prop
  method importation : bool t prop
  method notif : notif_js t prop
  method op : notif_manager_info_js t manager_operation_js t prop
  method op_bytes_ : js_string t prop
  method op_result_ : op_result t prop
  method ml_forge_result_ : Dune_types.node_operation_type Metal_types.forge_output prop
  method injecting : bool t prop
  method forging : bool t prop
  method notif_error_ : error_js t js_array t optdef prop
  method old_delegate_ : js_string t optdef prop
  method manage_account_admin_ : js_string t optdef prop
  method notarization_file_ : File.file_any opt prop
  method notarization_comments_ : js_string t prop
  method manage_account_json_ : File.file_any opt prop
  method transaction_history_ : history_js t prop
  method origination_history_ : history_js t prop
  method delegation_history_ : history_js t prop
  method notarization_history_ : history_js t prop
  method manage_account_history_ : history_js t prop
  method operation_tab_ : int prop
  method notarization_info_ : notarization_info_js t Js_of_ocaml.Jstable.t prop
  method copied : bool t prop
  method delegate : baker t optdef prop
  method bakers : baker t js_array t prop
  method notifs_list_visible_ : bool t prop
  method tr_expanded_ : bool t prop
  method del_expanded_ : bool t prop
  method postpone_time_ : int prop
  method batch_notif_ : bool t prop
end

class type data_js = object
  method path : js_string t prop
  method enabled : bool t prop
  method start : start_js t prop
  method pres : pres_js t prop
  method unlock : data_js t unlock_js t prop
  method reset : reset_js t prop
  method settings : settings_js t prop
  method core : core_js t prop
  method theme : theme_js t prop
end
