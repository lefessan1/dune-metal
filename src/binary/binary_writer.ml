(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open MCrypto

exception Forge_error of string

let char c = Bigstring.make 1 c
let uint8 i = char @@ char_of_int i
let bool b = if b then uint8 255 else uint8 0
let list l = Bigstring.concat "" l
let chars l = list (List.map char l)
let opt f = function
  | None -> bool false
  | Some x -> list [bool true; f x]

let int16 i = (* 2 bytes *)
  let x = i lsr 8 in
  list [
    uint8 (x land 0xff);
    uint8 (i land 0xff) ]

let int32 i = (* 4 bytes *)
  let rec f i acc x =
    let c = uint8 (x land 0xff) in
    if i = 3 then c :: acc
    else f (i+1) (c :: acc) (x lsr 8) in
  list (f 0 [] i)

let int64 i = (* 8 bytes *)
  let rec f i acc x =
    let c = uint8 Int64.(to_int @@ logand x 0xffL) in
    if i = 7 then c :: acc
    else f (i+1) (c :: acc) (Int64.shift_right x 8) in
  list (f 0 [] i)

let n_int64 z =
  let rec f acc x =
    if x < 0x80L then uint8 (Int64.to_int x) :: acc
    else f (uint8 Int64.(to_int (logor (logand x 0x7fL) 0x80L)) :: acc) (Int64.shift_right x 7) in
  list (List.rev @@ f [] z)

let n_zarith z =
  let rec f acc x =
    if x < Z.of_int 0x80 then uint8 (Z.to_int x) :: acc
    else f (uint8 Z.(to_int (logor (logand x (of_int 0x7f)) (of_int 0x80))) :: acc)
        (Z.shift_right x 7) in
  list (List.rev @@ f [] z)

let zarith z =
  let sign = z < Z.zero in
  let small = z < Z.of_int 0x40 in
  let z = Z.abs z in
  let first_byte = uint8 @@
    Z.to_int @@ Z.logor (Z.logand z (Z.of_int 0x3f)) @@
    Z.of_int @@ ((if sign then 0x40 else 0x00)
                 lor (if small then 0x00 else 0x80)) in
  if small then first_byte
  else list [first_byte; n_zarith @@ Z.shift_right z 6]

let elem e =
  list [int32 @@ Bigstring.length e; e]

let pkh pkh = (* 21 bytes *)
  let tag, prefix =
    if String.length pkh < 3 then
      raise @@ Forge_error (Printf.sprintf "wrong format for pkh: %S" pkh)
    else match String.sub pkh 0 3 with
      | "dn1" -> 0, Prefix.ed25519_public_key_hash
      | "dn2" -> 1, Prefix.secp256k1_public_key_hash
      | "dn3" -> 2, Prefix.p256_public_key_hash
      | _ -> raise @@ Forge_error (Printf.sprintf "wrong format for pkh: %S" pkh) in
  list [ uint8 tag; Base58.decode prefix pkh ]

let contract h = (* 22 bytes *)
  if h.[0] = 'd' then list [ uint8 0; pkh h]
  else
    list [
      uint8 1;
      Base58.decode Prefix.contract_public_key_hash h;
      uint8 0 ]

let source_manager h = pkh h

let pk pk = (* 33 or 34 bytes *)
  let tag, prefix =
    if String.length pk < 4 then
      raise @@ Forge_error (Printf.sprintf "wrong format for pk: %S" pk)
    else match String.sub pk 0 4 with
      | "edpk" -> 0, Prefix.ed25519_public_key
      | "sppk" -> 1, Prefix.secp256k1_public_key
      | "p2pk" -> 2, Prefix.p256_public_key
      | _ -> raise @@ Forge_error (Printf.sprintf "wrong format for pkh: %S" pk) in
  list [ uint8 tag; Base58.decode prefix pk ]

let branch hash = (* 32 bytes *)
  Base58.decode Prefix.block_hash hash

let signature s =
  let prefix =
    if String.length s < 3 then
      raise @@ Forge_error (Printf.sprintf "wrong format for signature: %S" s)
    else if String.sub s 0 3 = "sig" then Prefix.generic_signature
    else (
      if String.length s < 5 then
        raise @@ Forge_error (Printf.sprintf "wrong format for signature: %S" s)
      else (match String.sub s 0 5 with
          | "edsig" -> Prefix.ed25519_signature
          | "spsig" -> Prefix.secp256k1_signature
          | "p2sig" -> Prefix.p256_signature
          | _ -> raise @@ Forge_error (Printf.sprintf "wrong format for signature: %S" s))) in
  Base58.decode prefix s

let script_expr_hash s =
  Base58.decode Prefix.script_expr_hash s
