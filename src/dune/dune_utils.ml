(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open MMisc
open Dune_types

let string_of_ballot_vote = function
  | Yay -> "Yay"
  | Nay -> "Nay"
  | Pass -> "Pass"

let ballot_of_string = function
  | "Yay" | "yay" -> Yay
  | "Pass"| "pass" -> Pass
  | "Nay" | "nay" -> Nay
  | _ -> Nay

let voting_period_kind_of_string = function
  | "proposal" -> NProposal
  | "testing_vote" -> NTesting_vote
  | "testing" -> NTesting
  | "promotion_vote" -> NPromotion_vote
  | _ -> assert false

let string_of_voting_period_kind = function
  | NProposal -> "proposal"
  | NTesting_vote -> "testing_vote"
  | NTesting -> "testing"
  | NPromotion_vote -> "promotion_vote"

let pp_voting_period_kind = function
  | NProposal -> "Proposal"
  | NTesting_vote -> "Exploration"
  | NTesting -> "Testing"
  | NPromotion_vote -> "Promotion"

let string_of_op_contents = function
  | NSeed_nonce_revelation _ | NDouble_baking_evidence _
  | NDouble_endorsement_evidence _ | NActivation _ -> "Anonymous"
  | NEndorsement _ -> "Endorsement"
  | NProposals _ -> "Proposals"
  | NBallot _ -> "Ballot"
  | NTransaction _ | NDelegation _
  | NOrigination _ | NReveal _ -> "Manager"
  | NActivate -> "Activate"
  | NActivate_testnet -> "Activate_testnet"
  | NActivate_protocol _ -> "Activate_protocol"
  | NManage_accounts _ -> "Manage_accounts"
  | NManage_account _ -> "Manage_account"
  | NClearDelegations _ -> "Clear_delegations"

let string_of_anonymous_op_contents = List.fold_left (fun acc op -> match op with
    | NActivation _ -> "Activation" :: acc
    | NDouble_endorsement_evidence _ -> "Double_endorsement_evidence" :: acc
    | NDouble_baking_evidence _ -> "Double_baking_evidence" :: acc
    | NSeed_nonce_revelation _ -> "Nonce" :: acc
    | _ -> acc) []

let string_of_manager_op_contents = List.fold_left (fun acc op -> match op with
    | NTransaction _ -> "Transaction" :: acc
    | NOrigination _ -> "Origination" :: acc
    | NReveal _ -> "Reveal" :: acc
    | NDelegation _ -> "Delegation" :: acc
    | NActivate_protocol _ -> "Activate_protocol" :: acc
    | NManage_accounts _ -> "Manage_accounts" :: acc
    | NManage_account _ -> "Manage_account" :: acc
    | NClearDelegations _ -> "Clear_delegations" :: acc
    | _ -> acc) []

let string_of_balance_update = function
  | Contract (s, i) -> Printf.sprintf "Contract %s %Ld" s i
  | Rewards (s, i1, i2) -> Printf.sprintf "Rewards %s %d %Ld" s i1 i2
  | Fees (s, i1, i2) -> Printf.sprintf "Fees %s %d %Ld" s i1 i2
  | Deposits (s, i1, i2) -> Printf.sprintf "Deposits %s %d %Ld" s i1 i2

let limits_of_operation = function
  | NTransaction {node_tr_fee = f; node_tr_gas_limit = g; node_tr_storage_limit = s; _}
  | NOrigination  {node_or_fee = f; node_or_gas_limit = g; node_or_storage_limit = s; _}
  | NReveal {node_rvl_fee = f; node_rvl_gas_limit = g; node_rvl_storage_limit = s; _}
  | NDelegation {node_del_fee = f; node_del_gas_limit = g; node_del_storage_limit = s; _}
  | NActivate_protocol {node_acp_fee = f; node_acp_gas_limit = g; node_acp_storage_limit = s; _}
  | NManage_accounts {node_macs_fee = f; node_macs_gas_limit = g; node_macs_storage_limit = s; _}
  | NManage_account {node_mac_fee = f; node_mac_gas_limit = g; node_mac_storage_limit = s; _}
  | NClearDelegations {node_cld_fee = f; node_cld_gas_limit = g; node_cld_storage_limit = s; _} ->
    Some (f, g, s)
  | _ -> None

let limits_of_operation_exn op =
  unopt_exn @@ limits_of_operation op

let limits_of_operations ops =
  List.fold_left (fun (acc_f, acc_g, acc_s) op ->
      let (f, g, s) = unopt (0L, Z.zero, Z.zero) (limits_of_operation op) in
      Int64.add acc_f f, Z.add acc_g g, Z.add acc_s s) (0L, Z.zero, Z.zero) ops

let get_manager_metadata = function
  | NTransaction { node_tr_metadata = meta; _}
  | NOrigination  { node_or_metadata = meta; _}
  | NReveal { node_rvl_metadata = meta; _}
  | NDelegation { node_del_metadata = meta; _}
  | NActivate_protocol { node_acp_metadata = meta; _}
  | NManage_accounts { node_macs_metadata = meta; _}
  | NManage_account { node_mac_metadata = meta; _}
  | NClearDelegations { node_cld_metadata = meta; _} -> meta
  | _ -> None

let get_manager_counter = function
  | NTransaction { node_tr_counter = counter; _}
  | NOrigination  { node_or_counter = counter; _}
  | NReveal { node_rvl_counter = counter; _}
  | NDelegation { node_del_counter = counter; _}
  | NActivate_protocol { node_acp_counter = counter; _}
  | NManage_accounts { node_macs_counter = counter; _}
  | NManage_account { node_mac_counter = counter; _}
  | NClearDelegations { node_cld_counter = counter; _} -> Some counter
  | _ -> None

let get_manager_src = function
  | NTransaction { node_tr_src = src; _}
  | NOrigination  { node_or_src = src; _}
  | NReveal { node_rvl_src = src; _}
  | NDelegation { node_del_src = src; _}
  | NActivate_protocol { node_acp_src = src; _}
  | NManage_accounts { node_macs_src = src; _}
  | NManage_account { node_mac_src = src; _}
  | NClearDelegations { node_cld_src = src; _} -> Some src
  | _ -> None

let update_limits ?fee ?gas_limit ?storage_limit ?collect_fee_gas op =
  let diff_size = match limits_of_operation op with
    | None -> 0
    | Some (f, g, s) ->
      (unoptf 0 (fun fee -> Binary_size.n_int64 fee - Binary_size.n_int64 f) fee) +
      (unoptf 0 (fun gas -> Binary_size.n_zarith gas - Binary_size.n_zarith g) gas_limit) +
      (unoptf 0 (fun storage -> Binary_size.n_zarith storage - Binary_size.n_zarith s) storage_limit) in
  let op = match op with
    | NTransaction tr ->
      let node_tr_fee = unopt tr.node_tr_fee fee in
      let node_tr_gas_limit = unopt tr.node_tr_gas_limit gas_limit in
      let node_tr_storage_limit = unopt tr.node_tr_storage_limit storage_limit in
      let node_tr_collect_fee_gas = match tr.node_tr_collect_fee_gas, collect_fee_gas with
        | Some _, Some fee | Some fee, _ -> Some fee
        | _ -> None in
      NTransaction {tr with node_tr_fee; node_tr_gas_limit; node_tr_storage_limit;
                            node_tr_collect_fee_gas}
    | NOrigination ori ->
      let node_or_fee = unopt ori.node_or_fee fee in
      let node_or_gas_limit = unopt ori.node_or_gas_limit gas_limit in
      let node_or_storage_limit = unopt ori.node_or_storage_limit storage_limit in
      NOrigination {ori with node_or_fee; node_or_gas_limit; node_or_storage_limit}
    | NDelegation del ->
      let node_del_fee = unopt del.node_del_fee fee in
      let node_del_gas_limit = unopt del.node_del_gas_limit gas_limit in
      let node_del_storage_limit = unopt del.node_del_storage_limit storage_limit in
      NDelegation {del with node_del_fee; node_del_gas_limit; node_del_storage_limit}
    | NReveal rvl ->
      let node_rvl_fee = unopt rvl.node_rvl_fee fee in
      let node_rvl_gas_limit = unopt rvl.node_rvl_gas_limit gas_limit in
      let node_rvl_storage_limit = unopt rvl.node_rvl_storage_limit storage_limit in
      NReveal {rvl with node_rvl_fee; node_rvl_gas_limit; node_rvl_storage_limit}
    | NActivate_protocol acp ->
      let node_acp_fee = unopt acp.node_acp_fee fee in
      let node_acp_gas_limit = unopt acp.node_acp_gas_limit gas_limit in
      let node_acp_storage_limit = unopt acp.node_acp_storage_limit storage_limit in
      NActivate_protocol {acp with node_acp_fee; node_acp_gas_limit; node_acp_storage_limit}
    | NManage_accounts macs ->
      let node_macs_fee = unopt macs.node_macs_fee fee in
      let node_macs_gas_limit = unopt macs.node_macs_gas_limit gas_limit in
      let node_macs_storage_limit = unopt macs.node_macs_storage_limit storage_limit in
      NManage_accounts {macs with node_macs_fee; node_macs_gas_limit; node_macs_storage_limit}
    | NManage_account mac ->
      let node_mac_fee = unopt mac.node_mac_fee fee in
      let node_mac_gas_limit = unopt mac.node_mac_gas_limit gas_limit in
      let node_mac_storage_limit = unopt mac.node_mac_storage_limit storage_limit in
      NManage_account {mac with node_mac_fee; node_mac_gas_limit; node_mac_storage_limit}
    | op -> op in
  op, diff_size

let update_counter counter = function
  | NTransaction tr -> NTransaction {tr with node_tr_counter = counter}
  | NOrigination ori -> NOrigination {ori with node_or_counter = counter}
  | NDelegation del -> NDelegation {del with node_del_counter = counter}
  | NReveal rvl -> NReveal {rvl with node_rvl_counter = counter}
  | NActivate_protocol acp -> NActivate_protocol {acp with node_acp_counter = counter}
  | NManage_accounts macs -> NManage_accounts {macs with node_macs_counter = counter}
  | NManage_account mac -> NManage_account {mac with node_mac_counter = counter}
  | op -> op

let update_counters counter ops =
  let _, l = List.fold_left (fun (counter, acc) op ->
      Z.succ counter, update_counter counter op :: acc) (Z.succ counter, []) ops in
  List.rev l

let node_errors_to_string status errors =
  let status = unopt "none" status in
  Format.kasprintf (fun s -> Error (Str_err s))
    "@[<v>\
     Status: %s@,\
     @[<v 2>Errors:@,%a@]\
     @]"
    status
    (Format.pp_print_list (fun ppf err ->
         Format.fprintf ppf "- %s (%s) : %s"
           err.node_err_kind
           err.node_err_id
           err.node_err_info
       )) errors

let check_operation_status op f =
  match get_manager_metadata op with
  | None | Some { manager_meta_operation_result = None ; _ } ->
    Error (Str_err "Operation without metadata")
  | Some { manager_meta_operation_result = Some ({ meta_op_status = Some "applied" ; _ } as op_res);
           manager_meta_internal_operation_results; _} ->
    f op_res manager_meta_internal_operation_results
  | Some { manager_meta_operation_result = Some op_res ; _ } ->
    node_errors_to_string op_res.meta_op_status op_res.meta_op_errors
