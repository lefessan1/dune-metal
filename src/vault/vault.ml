(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types
open MCrypto
open Ezjs_min
open Crypto_js
open Chrome_lwt
open MLwt

let pkh_from_sk sk =
  let pk = Sk.to_public_key sk in
  let pkh = Pk.hash pk in
  Pkh.b58enc pkh

module Lwt = struct

  let password () =
    Runtime.getBackgroundPage () >>= fun w ->
    let password = match Optdef.to_option (Unsafe.coerce w)##.password with
      | None -> None
      | Some pwd -> Some (to_string pwd) in
    return password

  let set_password password =
    Runtime.getBackgroundPage () >>= fun w ->
    let password_t = match password with
      | None -> undefined
      | Some pwd -> def (string pwd) in
    (Unsafe.coerce w)##.password := password_t;
    return password

  let lock () = set_password None
  let is_locked () =
    password () >>= function None -> return true | _ -> return false

  let encrypt_base ?password ?manager ~pkh sk =
    let manager = match manager with None -> pkh | Some manager -> manager in
    match password with
    | None -> return (Error (Str_err "Cannot encrypt in locked session"))
    | Some password ->
      let pbkdf2 = make_algorithm ~salt:(Bigstring.of_string pkh)
          ~hash:"SHA-512" ~iterations:4096 "PBKDF2" in
      let iv = randomValues_uint8 16 in
      let aes_gcm0 = make_algorithm ~length:256 "AES-GCM" in
      let aes_gcm1 = make_algorithm ~iv "AES-GCM" in
      importKey "raw" (Bigstring.of_string password) pbkdf2 false ["deriveKey"]
      >>=? fun key ->
      deriveKey pbkdf2 key aes_gcm0 false ["encrypt"] >>=? fun key ->
      encrypt ~algo:aes_gcm1 ~key sk >>|? fun esk ->
      Mhelpers.dummy_account ~vault:(Local (iv, esk)) ~manager pkh

  let encrypt ?manager ~pkh sk =
    password () >>= fun password ->
    encrypt_base ?password ?manager ~pkh sk

  let decrypt_base ?password ~pkh vault =
    let (iv, esk) = vault in
    match password with
    | None -> return (Error (Str_err "No password given"))
    | Some password ->
      let pbkdf2 = make_algorithm ~salt:(Bigstring.of_string pkh)
          ~hash:"SHA-512" ~iterations:4096 "PBKDF2" in
      let aes_gcm0 = make_algorithm ~length:256 "AES-GCM" in
      let aes_gcm1 = make_algorithm ~iv "AES-GCM" in
      importKey "raw" (Bigstring.of_string password) pbkdf2 false ["deriveKey"]
      >>=? fun key ->
      deriveKey pbkdf2 key aes_gcm0 false ["decrypt"] >>=? fun key ->
      decrypt ~algo:aes_gcm1 ~key esk >>|? fun sk ->
      Bigstring.sub sk 0 32

  let decrypt ~pkh ~vault =
    password () >>= fun password ->
    decrypt_base ?password ~pkh vault

  let unlock_first pwd = set_password (Some pwd)
  let unlock {vault; pkh; manager; _} password =
    match vault with
    | Local vault ->
      decrypt_base ~password ~pkh vault >>=? fun sk ->
      if pkh_from_sk sk = manager then (
        set_password (Some password) >>= fun _ ->
        return (Ok password))
      else
        return (Error (Str_err "Wrong password"))
    | Ledger (path, pwd) ->
      let check_pwd = Blake2b_20.hash_string [password; path] in
      if check_pwd = pwd then (
        set_password (Some password) >>= fun _ ->
        return (Ok password))
      else return (Error (Str_err "Wrong password"))

  let check_password pwd =
    password () >>= function
    | None -> return false
    | Some password -> return (password = pwd)

  let sign ?watermark {pkh; vault; _} bytes = match vault with
    | Local (iv, esk) ->
      decrypt ~pkh ~vault:(iv, esk) >>=? fun sk ->
      (match Forge.sign ?watermark ~sk bytes with
       | None -> return (Error (Str_err "cannot sign locally"))
       | Some signature -> return (Ok signature))
    | Ledger (path, _) ->
      Ledger_js.sign_bg path (Forge.to_hex bytes) >>|? fun signature ->
      Forge.of_hex signature

  let pk_of_vault {pkh; vault; _} = match vault with
    | Local (iv, esk) ->
      decrypt ~pkh ~vault:(iv, esk) >>|? fun sk ->
      Pk.b58enc @@ Sk.to_public_key sk
    | Ledger (path, _) ->
      Ledger_js.getAddress_bg path >>|? fst

  let hash_password ?pwd ~pkh path =
    (match pwd with
     | None -> password ()
     | Some password -> return (Some password)) >>= function
    | None -> return (Error (Str_err "No password given"))
    | Some password ->
      let pwd = Blake2b_20.hash_string [password; path] in
      return (Ok (Mhelpers.dummy_account ~vault:(Ledger (path, pwd)) pkh))

end

module Cb = struct
  let password f = async0 (Lwt.password ()) f

  let set_password ?callback password = async0_opt ?callback (Lwt.set_password password)

  let lock ?callback () = set_password ?callback None
  let is_locked f = password (function None -> f true | Some _ -> f false)

  let encrypt_base ?error ?password ?manager ~pkh sk f =
    async_req ?error (Lwt.encrypt_base ?password ?manager ~pkh sk) f

  let encrypt ?error ?manager ~pkh sk f =
    async_req ?error (Lwt.encrypt ?manager ~pkh sk) f

  let decrypt_base ?error ?password ~pkh vault f =
    async_req ?error (Lwt.decrypt_base ?password  ~pkh vault) f

  let decrypt ?error ~pkh ~vault f =
    async_req ?error (Lwt.decrypt ~pkh ~vault) f

  let unlock_first ?callback pwd = set_password ?callback (Some pwd)
  let unlock ?error ?callback account password =
    async_req_opt ?error ?callback (Lwt.unlock account password)

  let check_password pwd f =
    password (function None -> f false | Some password -> f (password = pwd))

  let sign ?error account bytes f =
    async_req ?error (Lwt.sign account bytes) f

  let pk_of_vault ?error account f =
    async_req ?error (Lwt.pk_of_vault account) f

  let hash_password ?error ?pwd ~pkh path f =
    async_req ?error (Lwt.hash_password ?pwd ~pkh path) f
end
