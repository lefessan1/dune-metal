(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)
open Ezjs_min_lwt
open MLwt
open Chrome_lwt

let mk_message ?data action path =
  let params : Ledger_bridge.ledgerParams t = Unsafe.obj [||] in
  params##.path := string path;
  (match data with None -> () | Some data -> params##.data := some (string data));
  let message : Ledger_bridge.ledgerMessage t = Unsafe.obj [||] in
  message##.action := string action;
  message##.params := params;
  message

let getAddress_bg path =
  let message = mk_message "getAddress" path in
  let info = Utils.Runtime.mk_connection_info "ledger" in
  let port = Runtime.connect ~info () in
  port##postMessage message;
  let timeout_prom, timeout_resolver = Lwt.wait () in
  let res =
    Promise.to_lwt_cb0 (fun cb -> Utils.Browser.addListener1 port##.onMessage cb) in
  ignore @@
  Dom_html.setTimeout
    (fun () ->
       Lwt.wakeup_later_result timeout_resolver (Ok (Unsafe.obj [||]))
    )
    15000. ;
  Lwt.pick [ res ; timeout_prom ] >>= fun r ->
  port##disconnect;
  match to_opt to_bool r##.success, to_opt to_string r##.data with
  | Some true, Some r ->
    let b = Forge.of_hex r in
    let length = int_of_char @@ Bigstring.get b 0 in
    let pk = Bigstring.sub b 2 (length-1) in
    let pkh = MCrypto.Pkh.b58enc @@ MCrypto.Pk.hash pk in
    let pk = MCrypto.Pk.b58enc pk in
    return (Ok (pk, pkh))
  | _ -> return (Error (Str_err "failed getAddress"))

let sign_bg path data =
  let message = mk_message ~data "sign" path in
  let info = Utils.Runtime.mk_connection_info "ledger" in
  let port = Runtime.connect ~info () in
  port##postMessage message;
  let timeout_prom, timeout_resolver = Lwt.wait () in
  let res =
    Promise.to_lwt_cb0 (fun cb -> Utils.Browser.addListener1 port##.onMessage cb) in
  ignore @@
  Dom_html.setTimeout
    (fun () ->
       Lwt.wakeup_later_result timeout_resolver (Ok (Unsafe.obj [||]))
    )
    60000. ;
  Lwt.pick [ res ; timeout_prom ] >>= fun r ->
  port##disconnect;
  match to_opt to_bool r##.success, to_opt to_string r##.data with
  | Some true, Some r -> return (Ok r)
  | _ -> return (Error (Str_err "failed sign"))
