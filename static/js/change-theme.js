
function change_theme(theme) {
  var theme = theme == "material" ? "material" : "default";
  var i=0, links, elt ;
  for (i=0, links = document.getElementsByTagName("link"); i < links.length ; i++) {
    elt = links[i];
    if ((elt.rel.indexOf( "stylesheet" ) != -1) && elt.title) {
      elt.disabled = true ;
      if ((elt.title == ('bootstrap-' + theme))) {
        elt.disabled = false ;
      }
    }
  }
}

function get_theme (f) {
  chrome.storage.sync.get("theme", function(r) { f(r.theme) })
}

get_theme(function(theme) {
  change_theme(theme);
});
