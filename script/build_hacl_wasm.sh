#!/bin/bash

path=src
output=dist/hacl.js
optimize="-O3"

while getopts o:p:O: flag; do
    case $flag in
        o) output=${OPTARG};;
        p) path=${OPTARG};;
        O) optimize="-O"${OPTARG};;
    esac
done

src=( kremlib.c FStar.c Hacl_Unverified_Random.c Hacl_SHA2_256.c Hacl_SHA2_512.c Hacl_HMAC_SHA2_256.c Hacl_Ed25519.c Hacl_Policies.c AEAD_Poly1305_64.c Hacl_Chacha20.c Hacl_Poly1305_32.c Hacl_Poly1305_64.c Hacl_Chacha20Poly1305.c Hacl_Salsa20.c NaCl.c Hacl_Curve25519.c )

src=(${src[@]/#/$path/})

emcc \
    $optimize \
    -o $output \
    ${src[@]} \
    -DKRML_NOUINT128 \
    -s ALLOW_MEMORY_GROWTH=1 \
    -s MODULARIZE=1 \
    -s EXPORT_NAME=hacl_loader \
    -s WASM_BIGINT=1 \
    -s EXPORTED_FUNCTIONS='[ "_malloc", "_free", "_randombytes", "_Hacl_HMAC_SHA2_256_hmac", "_Hacl_SHA2_256_init", "_Hacl_SHA2_256_update", "_Hacl_SHA2_256_update_last", "_Hacl_SHA2_256_finish", "_Hacl_SHA2_512_init", "_Hacl_SHA2_512_update", "_Hacl_SHA2_512_update_last", "_Hacl_SHA2_512_finish", "_Hacl_Curve25519_crypto_scalarmult", "_NaCl_crypto_secretbox_easy", "_NaCl_crypto_secretbox_open_detached", "_NaCl_crypto_box_beforenm", "_NaCl_crypto_box_easy_afternm", "_NaCl_crypto_box_open_easy_afternm", "_Hacl_Ed25519_secret_to_public", "_Hacl_Ed25519_sign", "_Hacl_Ed25519_verify"]'

sed -i '1s/^/\/\/Provides: hacl_loader const/' $output
sed -i '3i\
  var ArrayBuffer = joo_global_object.ArrayBuffer;\
  var Browser = joo_global_object.Browser;\
  var Buffer = joo_global_object.Buffer;\
  var Float32Array = joo_global_object.Float32Array;\
  var Float64Array = joo_global_object.Float64Array;\
  var Int16Array = joo_global_object.Int16Array;\
  var Int32Array = joo_global_object.Int32Array;\
  var Int8Array = joo_global_object.Int8Array;\
  var Promise = joo_global_object.Promise;\
  var TextDecoder = joo_global_object.TextDecoder;\
  var Uint16Array = joo_global_object.Uint16Array;\
  var Uint32Array = joo_global_object.Uint32Array;\
  var Uint8Array = joo_global_object.Uint8Array;\
  var WebAssembly = joo_global_object.WebAssembly;\
  var __dirname = joo_global_object.__dirname;\
  var __filename = joo_global_object.__filename;\
  var clearInterval = joo_global_object.clearInterval;\
  var console = joo_global_object.console;\
  var crypto = joo_global_object.crypto;\
  var fetch = joo_global_object.fetch;\
  var importScripts = joo_global_object.importScripts;\
  var print = joo_global_object.print;\
  var printErr = joo_global_object.printErr;\
  var process = joo_global_object.process;\
  var quit = joo_global_object.quit;\
  var read = joo_global_object.read;\
  var readbuffer = joo_global_object.readbuffer;\
  var readline = joo_global_object.readline;\
  var scriptArgs = joo_global_object.scriptArgs;\
  var setTimeout = joo_global_object.setTimeout;' $output
sed -e :a -e '$d;N;2,7ba' -e 'P;D' -i $output
