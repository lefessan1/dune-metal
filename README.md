# Dune Metal Browser Extension

Metal is a extension for accessing Dune Network dApp in your
browser.
It also allow users to manage an account like a wallet : account
creation, ledger support, all operation (transactions, delegations,
originations, etc.)

Metal is developed by [Origin Labs](https://origin-labs.com).

You can find the latest version of Metal on [our official
website](https://metal.dune.network/).

Metal supports Firefox, Google Chrome, and Chromium-based
browsers. We recommend using the latest available browser version.

Do not forget to follow our related social media accounts:

[Twitter](https://twitter.com/dune_network) or
[Medium](https://medium.com/dune-network) pages.

## Building

# System dependencies

```
  $ sudo apt-get install git make npm
```

# OPAM install and setup

```
  $ sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
  $ opam init -y --bare
```

# Sass install

```
  $ sudo npm install -g sass
```

# OCaml dependencies

Install ocaml dependencies using opam from metal directory

```
    $ make build-deps
    $ eval $(opam env)
```

Then initialize the submodule:

```
    $ make submodules
```

Finally build the project:

```
    $ make
```

You can then load the browser extension by using the `static`
directory

## Build API documentation

You need `jsdoc` for this :
```
    $ npm install jsdoc -g
```
Then you can call `jsdoc` to generate the files:
```
make api-doc
```

## Contributing

All contribution are welcome.
