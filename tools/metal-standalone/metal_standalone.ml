open Ezjs_min_lwt
open Metal
open Types
open MLwt
open Crypto

module Node = Node.Make(EzXhr_lwt)

class type error_js = object
  method code : int readonly_prop
  method content : js_string t readonly_prop
end

let logs s = Js_of_ocaml.Firebug.console##log (string s)
let log_error e =
  let code, content = error_content0 e in
  logs @@ spf "Error %d: %s" code content

let js_error e =
  let code, content = error_content0 e in
  let e = error_of_string content in
  e##.name := string @@ string_of_int code;
  e

let (>|?) v f = match v with
  | Error e -> Error (js_error e)
  | Ok x -> Ok (f x)

let (>>|?) v f = v >>= function
  | Error e -> return (Error (js_error e))
  | Ok x -> return (Ok (f x))

class type vault_js = object
  method path : js_string t optdef prop
  method sk : js_string t optdef prop
  method pk : js_string t optdef prop
  method pkh : js_string t optdef prop
end

class type manager_info = object
  method fee : js_string t optdef prop
  method counter : js_string t optdef prop
  method gasLimit : js_string t optdef prop
  method storageLimit : js_string t optdef prop
end

class type parameter_js = object
  method entrypoint : js_string t optdef prop
  method value : js_string t optdef prop
end

class type transaction_details_js = object
  method dst : js_string t optdef prop
  method amount : js_string t optdef prop
  method parameter : parameter_js t optdef prop
  method collect_call_ : bool t optdef prop
end

class type script_js = object
  method sc_code_ : js_string t optdef prop
  method sc_storage_ : js_string t optdef prop
  method sc_code_hash_ : js_string t optdef prop
end

class type origination_details_js = object
  inherit script_js
  method balance : js_string t optdef prop
end

class type reveal_details_js = object
  method pubkey : js_string t optdef prop
end

class type delegation_details_js = object
  method delegate : js_string t optdef prop
end

class type send_argument = object
  inherit manager_info
  inherit transaction_details_js
  inherit vault_js
  method network : js_string t optdef prop
end

class type originate_argument = object
  inherit manager_info
  inherit origination_details_js
  inherit vault_js
  method network : js_string t optdef prop
end

class type delegate_argument = object
  inherit manager_info
  inherit delegation_details_js
  inherit vault_js
  method network : js_string t optdef prop
end

class type operation_js = object
  inherit manager_info
  inherit transaction_details_js
  inherit origination_details_js
  inherit reveal_details_js
  inherit delegation_details_js
  method kind : js_string t optdef prop
end

class type inputs_js = object
  inherit vault_js
  method network : js_string t optdef prop
  method operations : operation_js t js_array t optdef prop
  method branch : js_string t optdef prop
  method bytes : js_string t optdef prop
end

class type rpc_argument = object
  method rpc : js_string t optdef prop
  method network : js_string t optdef prop
end

type vault =
  | Ledger of string
  | Local of (Bigstring.t option * string option * string option)

type metal_ops =
  (notif_manager_info,
   (Dune.Types.account_hash *
    (Bigstring.t -> (Bigstring.t, metal_error) result Lwt.t))
     option manager_details)
    manager_operation

type inputs = {
  base : EzAPI.TYPES.base_url;
  operations : metal_ops list option;
  branch : string option;
  bytes : Bigstring.t option;
  vault : vault
}

let result_optdef res f = match res with
  | Ok x -> def (f x)
  | Error e -> log_error e; undefined

let opt_exn field = function
  | None ->
    logs @@ spf "%S field required and not provided" field;
    assert false
  | Some x -> x

let get_manager_info op = {
  not_mi_fee = to_optdef (fun x -> Int64.of_string @@ to_string x) op##.fee;
  not_mi_gas_limit = to_optdef (fun x -> Z.of_string @@ to_string x) op##.gasLimit;
  not_mi_storage_limit = to_optdef (fun x -> Z.of_string @@ to_string x) op##.storageLimit;
}

let amount_of_string ?(factor=1.) s_js =
  let s = String.lowercase_ascii @@ to_string s_js in
  if s = "max" || s = "maximum" then Int64.minus_one
  else match float_of_string_opt s with
    | None -> log_str "cannot read amount"; assert false
    | Some f -> Int64.of_float @@ f *. factor

let get_transaction_details op = {
  trd_dst = opt_exn "dst" @@ to_optdef to_string op##.dst;
  trd_amount = opt_exn "amount" @@ to_optdef (amount_of_string ~factor:1000000.) op##.amount;
  trd_parameters = to_optdef (fun p ->
      to_optdef to_string p##.entrypoint,
      to_optdef to_string p##.value) op##.parameter;
  trd_collect_call = unoptdef_f false to_bool op##.collect_call_
}

let get_origination_details op = {
  ord_balance = opt_exn "balance" @@ to_optdef (amount_of_string ~factor:1000000.) op##.balance;
  ord_script = (match to_optdef to_string op##.sc_storage_ with
      | Some storage ->
        Some (to_optdef to_string op##.sc_code_, storage,
              to_optdef to_string op##.sc_code_hash_)
      | None -> None);
  ord_delegate = None;
  ord_kt1 = None
}

let get_reveal_details op =
  opt_exn "pubkey" @@ to_optdef to_string op##.pubkey

let get_delegation_details op =
  to_optdef to_string op##.delegate

let to_metal (op : operation_js t) =
  let mo_info = get_manager_info op in
  let mo_det = match opt_exn "kind" @@ to_optdef to_string op##.kind with
    | "transaction" -> Some (TraDetails (get_transaction_details op))
    | "origination" -> Some (OriDetails (get_origination_details op))
    | "reveal" -> Some (RvlDetails (get_reveal_details op))
    | "delegation" -> Some (DelDetails (get_delegation_details op))
    | _ -> None in
  match mo_det with
  | None -> Error (Str_err "Operation not handled")
  | Some mo_det -> Ok {mo_info; mo_det}

let get_node_url node =
  let default = EzAPI.TYPES.BASE "https://mainnet-node.dunscan.io/" in
  match to_optdef to_string node with
  | None -> default
  | Some node ->
    match Url.url_of_string node with
    | Some _ -> EzAPI.TYPES.BASE node
    | None -> match node with
      | "mainnet" | "Mainnet" -> default
      | "testnet" | "Testnet" -> EzAPI.TYPES.BASE "https://testnet-node.dunscan.io/"
      | _ -> default

let get_vault vault =
  match to_optdef to_string vault##.path with
  | Some path -> Ledger path
  | None -> Local (
      to_optdef (fun s -> Sk.b58dec @@ to_string s) vault##.sk,
      to_optdef to_string vault##.pk,
      to_optdef to_string vault##.pkh)

let get_inputs (inputs : inputs_js t) =
  let base = get_node_url inputs##.network in
  let operations = match Optdef.to_option inputs##.operations with
    | None -> None
    | Some ops ->
      match map_res to_metal (to_list ops) with
      | Error e -> log_error e; None
      | Ok ops -> Some ops in
  let vault = get_vault inputs in
  let branch = to_optdef to_string inputs##.branch in
  let bytes = to_optdef (fun s -> Forge.of_hex (to_string s)) inputs##.bytes in
  { base; operations; vault; branch; bytes }

let get_send_argument (arg : send_argument t) =
  let base = get_node_url arg##.network in
  let vault = get_vault arg in
  let operations = Some
      [ { mo_info = get_manager_info arg;
          mo_det = TraDetails (get_transaction_details arg) } ] in
  let branch, bytes = None, None in
  { base; operations; vault; branch; bytes }

let get_originate_argument (arg : originate_argument t) =
  let base = get_node_url arg##.network in
  let vault = get_vault arg in
  let operations = Some
      [ { mo_info = get_manager_info arg;
          mo_det = OriDetails (get_origination_details arg) } ] in
  let branch, bytes = None, None in
  { base; operations; vault; branch; bytes }

let get_delegate_argument (arg : delegate_argument t) =
  let base = get_node_url arg##.network in
  let vault = get_vault arg in
  let operations = Some
      [ { mo_info = get_manager_info arg;
          mo_det = DelDetails (get_delegation_details arg) } ] in
  let branch, bytes = None, None in
  { base; operations; vault; branch; bytes }

let sign_from_vault ?watermark vault bytes = match vault with
  | Local (sk, _, _) ->
    let sk = opt_exn "sk" sk in
    begin match Forge.sign ?watermark ~sk bytes with
      | None -> Lwt.return (Error (Str_err "cannot sign locally"))
      | Some signature -> Lwt.return (Ok signature) end
  | Ledger path ->
    Lwt.map
      (function
        | Ok signature -> Ok (Forge.of_hex signature)
        | Error e -> Error e)
      (Ledger_js.sign_bg path (Forge.to_hex bytes))

let pk_of_vault = function
  | Local (sk, pk, _) ->
    begin match pk with
      | Some pk -> Lwt.return (Ok pk)
      | None -> begin match sk with
          | None -> Lwt.return @@ Error (Str_err "pk or sk not provided")
          | Some sk -> Lwt.return @@ Ok (Pk.b58enc @@ Sk.to_public_key sk)
        end
    end
  | Ledger path ->
    Lwt.map (function Ok x -> Ok (fst x) | Error e -> Error e) (Ledger_js.getAddress_bg path)

let pkh_of_vault = function
  | Local (sk, pk, pkh) ->
    begin match pkh with
      | Some pkh -> Lwt.return (Ok pkh)
      | None ->
        begin match pk with
          | Some pk -> Lwt.return (Ok (Pkh.b58enc @@ Pk.(hash @@ b58dec pk)))

          | None -> begin match sk with
              | None -> Lwt.return @@ Error (Str_err "pkh or pk or sk not provided")
              | Some sk -> Lwt.return @@ Ok (Pkh.b58enc @@ Pk.(hash @@ Sk.to_public_key sk))
            end
        end
    end
  | Ledger path ->
    Lwt.map (function Ok x -> Ok (snd x) | Error e -> Error e) (Ledger_js.getAddress_bg path)

let sign inputs =
  let inputs = get_inputs inputs in
  let bytes = opt_exn "bytes" inputs.bytes in
  Promise.promise_lwt_res
    (sign_from_vault inputs.vault bytes >>|? (fun b -> string @@ Forge.to_hex b))

let forge_operation_base inputs =
  let inputs = get_inputs inputs in
  let ops = opt_exn "operations" inputs.operations in
  Promise.promise_lwt_res
    (Lwt.map (function
         | Error e -> Error (js_error e)
         | Ok src ->
           match List.map (To_dune.dune_op_base ~src) ops with
           | [ op ] -> Forge.forge_operation op >|? fun b ->
             string @@ Forge.to_hex b
           | _ ->
             Error (js_error (Str_err "Error: several operations given")))
         (pkh_of_vault inputs.vault))

let forge_operations_base inputs =
  let inputs = get_inputs inputs in
  let branch = opt_exn "branch" inputs.branch in
  let ops = opt_exn "operations" inputs.operations in
  Promise.promise_lwt_res (
    pkh_of_vault inputs.vault >>=? (fun src ->
        let ops = List.map (To_dune.dune_op_base ~src) ops in
        Lwt.return @@ Forge.forge_operations branch ops) >>|? fun b ->
    string @@ Forge.to_hex b)

let send_bytes inputs =
  let inputs = get_inputs inputs in
  let base = inputs.base in
  let bytes = opt_exn "bytes" inputs.bytes in
  Promise.promise_lwt_res (
    Node.silent_inject ~base bytes >>|? fun op_bytes ->
    string @@ Operation_hash.b58enc op_bytes)

let forge_manager_operations inputs =
  let inputs = get_inputs inputs in
  let base = inputs.base in
  let ops = opt_exn "operations" inputs.operations in
  let get_pk () = pk_of_vault inputs.vault in
  Promise.promise_lwt_res (
    pkh_of_vault inputs.vault >>=? (fun src ->
        Node.forge_manager_operations ~base ~get_pk ~src ops) >>|? fun {fg_bytes; _} ->
    string @@ Forge.to_hex fg_bytes)

let send_manager_operations_base inputs =
  let base = inputs.base in
  let ops = opt_exn "operations" inputs.operations in
  let get_pk () = pk_of_vault inputs.vault in
  Promise.promise_lwt_res (
    pkh_of_vault inputs.vault >>=? (fun src ->
        Node.forge_manager_operations ~base ~get_pk ~src ops) >>=? (fun fg ->
        Node.inject ~base ~sign:(sign_from_vault inputs.vault) fg) >>|? fun op_bytes ->
    string @@ Operation_hash.b58enc op_bytes)

let send_manager_operations inputs =
  let inputs = get_inputs inputs in
  send_manager_operations_base inputs

let send arg =
  let inputs = get_send_argument arg in
  send_manager_operations_base inputs

let originate arg =
  let inputs = get_originate_argument arg in
  send_manager_operations_base inputs

let delegate arg =
  let inputs = get_delegate_argument arg in
  send_manager_operations_base inputs

let rpc (arg : rpc_argument t) =
  let base = get_node_url arg##.network in
  let rpc = opt_exn "rpc" (to_optdef to_string arg##.rpc) in
  Promise.promise_lwt_res (
    Node.get_raw_rpc ~base rpc >>|? fun s ->
    _JSON##parse (string s))

let rpc_base url f arg =
  let base = get_node_url (match arg with None -> undefined | Some arg -> arg##.network) in
  Promise.promise_lwt_res (
    Node.get_raw_rpc ~base url >>|? fun s ->
    f (_JSON##parse (string s)))

let head arg = rpc_base "chains/main/blocks/head" (fun x -> x) arg
let branch arg = rpc_base "chains/main/blocks/head" (fun x -> x) arg
let level arg = rpc_base "chains/main/blocks/head/header" (fun x -> x##.level) arg
let account_info arg =
  let pkh = to_string arg##.pkh in
  rpc_base
    (Format.sprintf "chains/main/blocks/head/context/contracts/%s/info" pkh)
    (fun x -> x) (Some arg)

let create_account arg =
  let mnemonic = match Optdef.to_option  arg##.mnemonic with
    | Some a -> to_listf to_string a
    | None ->
      let x = Hacl.Rand.gen 20 in
      Bip39.to_words @@ Bip39.of_entropy x in
  let passphrase = match Optdef.to_option arg##.passphrase with
    | Some s -> Bigstring.of_string @@ to_string s
    | None -> Bigstring.empty in
  let indices = Bip39.of_words mnemonic in
  match indices with
  | None -> assert false
  | Some indices ->
    let sk = Bigstring.sub (Bip39.to_seed ~passphrase indices) 0 32 in
    let pk = Sk.to_public_key sk in
    let pkh = Pk.hash pk in
    object%js
      val mnemonic = of_listf string mnemonic
      val passphrase = string (Bigstring.to_string passphrase)
      val sk = string @@ Sk.b58enc sk
      val pk = string @@ Pk.b58enc pk
      val pkh = string @@ Pkh.b58enc pkh
    end

let () =
  Ledger_bridge_js.make_iframe ();
  export "metal" @@ object%js
    method sign inputs = sign inputs
    method forge_operation_ inputs = forge_operation_base inputs
    method forge_operations_raw_ inputs = forge_operations_base inputs
    method forge_operations inputs = forge_manager_operations inputs
    method send_bytes_ inputs = send_bytes inputs
    method send arg = send arg
    method originate arg = originate arg
    method delegate arg = delegate arg
    method batch inputs = send_manager_operations inputs
    method rpc arg = rpc arg
    method head arg = head @@ Optdef.to_option arg
    method branch arg = branch @@ Optdef.to_option arg
    method level arg = level @@ Optdef.to_option arg
    method account_info_ arg = account_info arg
    method create_account_ arg = create_account arg
  end
