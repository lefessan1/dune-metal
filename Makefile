RLSDIR:=releases/dune-metal
-include Makefile.config
NPM = $(shell npm config get prefix)

all: build copy-js

dev: build-dev copy-js

build:
	NODE_PATH=$(NPM)/lib/node_modules dune build

build-dev:
	METAL_DEV=true NODE_PATH=$(NPM)/lib/node_modules dune build

copy-js: static/css/main.css
	@cp -fp _build/default/src/ui/metal_popup.bc.js static/metal-popup.js
	@cp -fp _build/default/src/ui/metal_notification.bc.js static/metal-notif.js
	@cp -fp _build/default/src/background/background.bc.js static/background.js
	@cp -fp _build/default/src/background/contentscript.bc.js static/contentscript.js
	@cp -fp _build/default/src/ui/metal_home.bc.js static/metal-home.js
	@cp -ufp $(OPAM_SWITCH_PREFIX)/share/hacl/hacl.wasm wasm/hacl.wasm

static/css/main.css: static/scss/main.scss
	@sass static/scss/main.scss static/css/main.css

build-deps:
	@opam install . --deps-only --working-dir -y

submodules:
	git submodule update --init

release:
	@-mkdir -p $(RLSDIR)
	cp -r static/* $(RLSDIR)

api-doc:
	jsdoc static/inpage.js -d docs -R README.md -u static/tutorials

clean:
	dune clean

standalone: build
	@cp -frL _build/install/default/share/metal dist

node-deps:
	@sudo npm install -g fs vue-template-compiler @vue/component-compiler-utils
